import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./ui/theme";
import TopNav from "./navigation/topnav/TopNav";
import Menu from "./navigation/menu/Menu";
import Footer from "./navigation/footer/Footer";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Offers from "./pages/offers/Offers";
import { AppBar, Box } from "@mui/material";
import ProductProvider from "./providers/ProductProvider";
import CartProvider from "./providers/CartProvider";
import { FallBack } from "./ui/fallback/FallBack";
import AuthProvider from "./providers/AuthProvider";
import UserProvider from "./providers/UserProvider";
import { NotFound } from "./ui/NotFound";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

function App() {
  const Register = lazy(() => import("./pages/register/Register"));
  const Products = lazy(() => import("./pages/products/Products"));
  const SingleProduct = lazy(() =>
    import("./pages/singleproduct/SingleProduct")
  );
  const Contact = lazy(() => import("./pages/contact/Contact"));
  const About = lazy(() => import("./pages/about/About"));
  const Faq = lazy(() => import("./pages/faq/Faq"));
  const CheckOut = lazy(() => import("./pages/checkout/Checkout"));
  const UserDetails = lazy(() => import("./pages/userDetails/UserDetails"));

  const options = {
    position: "top center",
    timeout: 3000,
    containerStyle: {
      zIndex: theme.zIndex.drawer + 5,
    },
    transition: transitions.FADE,
  };

  return (
    <>
      <Router>
        <ThemeProvider theme={theme}>
          <AlertProvider template={AlertTemplate} {...options}>
            <AuthProvider>
              <UserProvider>
                <ProductProvider>
                  <CartProvider>
                    <AppBar
                      position="fixed"
                      elevation={0}
                      sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    >
                      <TopNav />
                      <Menu />
                    </AppBar>
                    <Box height={90}></Box>
                    <Routes>
                      <Route path="/" element={<Home />} />
                      <Route
                        path="/register"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <Register />
                          </Suspense>
                        }
                      />
                      <Route path="/login" element={<Login />} />
                      <Route
                        path="/contact"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <Contact />
                          </Suspense>
                        }
                      />
                      <Route
                        path="/products"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <Products />
                          </Suspense>
                        }
                      />
                      <Route
                        path="/products/:id"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <SingleProduct />
                          </Suspense>
                        }
                      />
                      <Route path="/offers" element={<Offers />} />
                      <Route
                        path="/checkout"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <CheckOut />
                          </Suspense>
                        }
                      />
                      <Route
                        path="/user"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <UserDetails />
                          </Suspense>
                        }
                      />
                      <Route
                        path="/about"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <About />
                          </Suspense>
                        }
                      />
                      <Route
                        path="/faq"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <Faq />
                          </Suspense>
                        }
                      />
                      <Route
                        path="*"
                        element={
                          <Suspense fallback={<FallBack />}>
                            <NotFound />
                          </Suspense>
                        }
                      />
                    </Routes>
                    <Footer />
                  </CartProvider>
                </ProductProvider>
              </UserProvider>
            </AuthProvider>
          </AlertProvider>
        </ThemeProvider>
      </Router>
    </>
  );
}

export default App;
