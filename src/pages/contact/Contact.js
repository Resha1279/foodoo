import React, { useState } from "react";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import {
  Box,
  Grid,
  Typography,
  Paper,
  TextField,
  Button,
  Alert,
  Snackbar,
} from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
import Title from "../../ui/title/Title";
import GoogleMapReact from "google-map-react";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EmailIcon from "@mui/icons-material/Email";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import { send } from "emailjs-com";
import "./contact.css";
import { init } from "emailjs-com";
import CustomAlert from "../../ui/alert/Alert";
init("user_dqbja5U7nzc4DtC2C7zBh");

const Contact = () => {
  useDocumentTitle("Contact");

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [openAlert, setOpenAlert] = useState(false);
  const [isLoading, setIsLoading] = useState("");

  const location = {
    address: "E.K. Solutions Pvt Ltd",
    lat: 27.685497819246628,
    lng: 85.32026958465741,
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    send(
      "service_512uxx4",
      "template_x1a5wdk",
      {
        from_name: name,
        to_name: "Resha Adhikari",
        message: message,
        reply_to: email,
      },
      "user_dqbja5U7nzc4DtC2C7zBh"
    )
      .then((response) => {
        console.log("SUCCESS!", response.status, response.text);
        setOpenAlert(true);
        setName("");
        setEmail("");
        setMessage("");
      })
      .catch((err) => {
        console.log("FAILED...", err);
      });
  };

  const handleClose = (event, reason) => {
    setOpenAlert(false);
  };
  return (
    <Box>
      <Box className="container" p={5}>
        <Grid container spacing={3}>
          <Grid position="relative" item xs={12} lg={8}>
            <Map location={location} />
            <Box className="CONTACT-info">
              <Box className="CONTACT-info-content">
                <Box p={5}>
                  <Typography variant="h5" fontWeight="bold" gutterBottom>
                    Contact Info
                  </Typography>
                  <Typography
                    fontWeight="bold"
                    color="common.white"
                    gutterBottom
                  >
                    1234k Avenue, 4th block, New York City.
                  </Typography>

                  <Grid container alignItems="center" mt={1} spacing={2}>
                    <Grid item>
                      <EmailIcon color="primary" />
                    </Grid>
                    <Grid item>
                      <Typography color="common.white" gutterBottom>
                        info@example.com
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid container alignItems="center" mt={1} spacing={2}>
                    <Grid item>
                      <LocalPhoneIcon color="primary" />
                    </Grid>
                    <Grid item>
                      <Typography color="common.white" gutterBottom>
                        +(0123) 232 232
                      </Typography>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} lg={4}>
            <Box>
              <Title text="leave a message" />
            </Box>
            <Box>
              <Grid align="center">
                <Box sx={{ padding: "40px" }}>
                  <form action="submit" onSubmit={handleSubmit}>
                    <TextField
                      label="name"
                      type="text"
                      required
                      placeholder="Enter Name"
                      fullWidth
                      sx={{ marginBottom: "20px" }}
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                    />
                    <TextField
                      label="Email"
                      type="email"
                      placeholder="Enter Email"
                      required
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      fullWidth
                      sx={{ marginBottom: "20px" }}
                    />
                    <TextField
                      label="Message"
                      placeholder="Your Message Here..."
                      multiline
                      required
                      rows={4}
                      value={message}
                      onChange={(e) => {
                        setMessage(e.target.value);
                      }}
                      fullWidth
                      sx={{ marginBottom: "20px" }}
                    />

                    <Button type="submit" variant="contained" fullWidth>
                      Submit
                    </Button>
                  </form>
                </Box>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </Box>
      <CustomAlert
        openAlert={openAlert}
        handleClose={handleClose}
        message=" Your Message has been sent. We will reach out to you soon. Happy
          Shopping!"
      />
    </Box>
  );
};

export default Contact;

const Map = ({ location }) => (
  <div className="map">
    <div className="google-map">
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyAMnpW1vhiI6mpHLKTT7aDGGcDQdGgkOZE" }}
        defaultCenter={location}
        defaultZoom={15}
      >
        <LocationPin
          lat={location.lat}
          lng={location.lng}
          text={location.address}
        />
      </GoogleMapReact>
    </div>
  </div>
);

const LocationPin = ({ text }) => (
  <div className="pin">
    <LocationOnIcon />
    <p className="pin-text">{text}</p>
  </div>
);
