import React, { useState } from "react";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import Title from "../../ui/title/Title";
import {
  Avatar,
  FormControl,
  Paper,
  TextField,
  Typography,
  Box,
  Button,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import "./register.css";
import useForm from "../../hooks/useForm";
import { BASE_URL } from "../../constants/constants";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  useDocumentTitle("Kitchen In Cloud | Register");

  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const register = () => {
    console.log(inputValues);
    setIsLoading(true);
    setError(false);

    let url = `${BASE_URL}/api/v4/auth/signup`;

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(inputValues),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("sign in Success:", result);
        setIsLoading(false);
        setIsLoading(false);
        window.scrollTo({
          top: 0,
          behavior: "smooth",
        });
        navigate("/login");
      })
      .catch((error) => {
        setIsLoading(false);
        setError(true);
        setErrorMessage("The email or mobile number has already been taken");

        console.error("Error:", error);
      });
  };

  const [
    inputValues,
    handleChange,
    handleSubmit,
    initiateValidityCheck,
    valid,
    helperText,
  ] = useForm(register);

  return (
    <Box className="REGISTER-container">
      <Title text="Create New Account" />
      <Box
        className="container"
        my={5}
        display="flex"
        sx={{ justifyContent: { xs: "center", md: "end" } }}
      >
        <Paper
          align="center"
          sx={{ padding: 3, width: { xs: 300, md: 400, lg: 500 } }}
        >
          <Avatar>
            <LockOutlinedIcon />
          </Avatar>
          <Typography m={2} variant="h5">
            Register
          </Typography>
          {error && (
            <Typography variant="overline" color="error">
              {errorMessage}
            </Typography>
          )}
          <form
            noValidate
            autoComplete="off"
            action="submit"
            onSubmit={handleSubmit}
          >
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                name="first_name"
                label="First Name"
                type="text"
                placeholder="Enter First Name"
                required
                error={initiateValidityCheck.first_name && !valid.first_name}
                helperText={helperText.first_name}
                value={inputValues.first_name}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                name="last_name"
                label="Last Name"
                type="text"
                placeholder="Enter Last Name"
                required
                error={initiateValidityCheck.last_name && !valid.last_name}
                helperText={helperText.last_name}
                value={inputValues.last_name}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                name="email"
                label="Email"
                type="email"
                placeholder="Enter Email"
                required
                error={initiateValidityCheck.email && !valid.email}
                helperText={helperText.email}
                value={inputValues.email}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                name="mobile_number"
                label="Mobile Number"
                type="number"
                placeholder="Enter Mobile Number"
                required
                error={
                  initiateValidityCheck.mobile_number && !valid.mobile_number
                }
                helperText={helperText.mobile_number}
                value={inputValues.mobile_number}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </FormControl>
            <FormControl sx={{ width: "90%", margin: "10px" }}>
              <TextField
                name="password"
                label="Password"
                type="password"
                placeholder="Enter Password"
                required
                error={initiateValidityCheck.password && !valid.password}
                helperText={helperText.password}
                value={inputValues.password}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </FormControl>

            <LoadingButton
              loading={isLoading}
              type="submit"
              variant="contained"
              sx={{ margin: "10px" }}
            >
              Sign Up
            </LoadingButton>
          </form>
          <Typography color="GrayText">Already have an account?</Typography>
          <Link to="/login">
            <Button color="info">Go to login</Button>
          </Link>
        </Paper>
      </Box>
    </Box>
  );
};

export default Register;
