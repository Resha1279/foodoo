import Title from "../../ui/title/Title";
import React from "react";
import Carousel from "react-material-ui-carousel";
import "./about.css";
import { Grid, Box, Typography } from "@mui/material";
import useDocumentTitle from "../../hooks/useDocumentTitle";

const About = () => {
  useDocumentTitle("About Us");
  return (
    <>
      <Title text="About Us" width="60px" />
      <Box className="container">
        <Grid container spacing={3}>
          <Grid item xs={12} lg={8}>
            <Typography variant="h5" fontWeight="bold" py={3}>
              Blanditiis praesentium deleniti atque corrupti quos
            </Typography>
            <Typography variant="body1" pb={3} color="GrayText">
              Gnissimos voluptatum deleniti atque corrupti quos dolores et quas
              molestias excepturi. atque corrupti quos dolores et quas molestias
              excepturi sint occaecat officia deserunt mollitia laborum et
              dolorum fuga
            </Typography>
            <Typography variant="body1" gutterBottom color="GrayText">
              Dignissimos at vero eos et accusamus et iusto odio ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecat officia deserunt
              mollitia laborum et dolorum fuga. At vero eos et accusamus et
              iusto odio dignissimos ducimus qui blanditiis praesentium
              voluptatum deleniti atque corrupti quos dolores et quas molestias
              excepturi sint occaecat atque corrupti quos dolores et quas
              molestias excepturi sint occaecat officia deserunt mollitia
              laborum et dolorum
            </Typography>
          </Grid>
          <Grid item xs={12} lg={4}>
            <Box sx={{ backgroundColor: "#212121" }}>
              <Box sx={{ backgroundColor: "primary.main" }}>
                <Typography variant="h4" fontWeight="bold" align="center" p={2}>
                  Praesentium :
                </Typography>
              </Box>
              <Typography
                variant="h6"
                fontWeight="bold"
                align="center"
                color="common.white"
                p={2}
              >
                At vero eos
              </Typography>
              <Typography
                variant="h6"
                fontWeight="bold"
                align="center"
                color="common.white"
              >
                Accusamus et
              </Typography>
              <Typography
                variant="body1"
                color="lightgray"
                align="center"
                p={2}
              >
                Dignissimos at vero eos et accusamus et iusto odio ducimus qui
                accusamus et.
              </Typography>
            </Box>
          </Grid>
          <Grid item container xs={12} lg={8} direction="row">
            <Grid item xs={4}>
              <img width="100%" src="/assets/gallery3.jpg" alt="gallery3" />
            </Grid>
            <Grid item xs={4}>
              <img width="100%" src="/assets/gallery4.jpg" alt="gallery4" />
            </Grid>
            <Grid item xs={4}>
              <img width="100%" src="/assets/gallery3.jpg" alt="gallery3" />
            </Grid>
          </Grid>
          <Grid item xs={12} lg={4}>
            <Title text="Testimonial" width="50px" />
            <Carousel>
              <Box sx={{ backgroundColor: "info.main" }}>
                <Typography
                  variant="h6"
                  fontWeight="bold"
                  color="common.white"
                  p={3}
                  pt={5}
                >
                  " I AM LOREM IPSUM.
                </Typography>

                <Typography variant="subtitle2" px={3}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse eu magna dolor, quisque semper.
                </Typography>

                <Typography p={3} pb={5} fontWeight="bold">
                  Amet Doe,&nbsp;
                  <Typography display="inline" variant="subtitle2">
                    Adipiscing
                  </Typography>
                </Typography>
              </Box>
              <Box sx={{ backgroundColor: "info.main" }}>
                <Typography
                  variant="h6"
                  fontWeight="bold"
                  color="common.white"
                  p={3}
                  pt={5}
                >
                  " I AM VERY PLEASED.
                </Typography>

                <Typography variant="subtitle2" px={3}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse eu magna dolor, quisque semper.
                </Typography>

                <Typography p={3} pb={5} fontWeight="bold">
                  Elit Sempe,&nbsp;
                  <Typography display="inline" variant="subtitle2">
                    Suspendisse
                  </Typography>
                </Typography>
              </Box>
              <Box sx={{ backgroundColor: "info.main" }}>
                <Typography
                  variant="h6"
                  fontWeight="bold"
                  color="common.white"
                  p={3}
                  pt={5}
                >
                  " CONSECTETUR PIMAGNA.
                </Typography>

                <Typography variant="subtitle2" px={3}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Suspendisse eu magna dolor, quisque semper.
                </Typography>

                <Typography p={3} pb={5} fontWeight="bold">
                  John Doe,&nbsp;
                  <Typography display="inline" variant="subtitle2">
                    Dolor Elit
                  </Typography>
                </Typography>
              </Box>
            </Carousel>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default About;
