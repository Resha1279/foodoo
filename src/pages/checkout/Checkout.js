import React, { useContext, useEffect, useState } from "react";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import {
  Box,
  Typography,
  Grid,
  IconButton,
  Button,
  Modal,
  Divider,
  FormControl,
  Select,
  MenuItem,
  InputLabel,
} from "@mui/material";
import Title from "../../ui/title/Title";
import cartContext from "../../contexts/cart-context";
import { DataGrid } from "@mui/x-data-grid";
import DeleteIcon from "@mui/icons-material/Delete";
import "./checkout.css";
import authContext from "../../contexts/auth-context";
import CartForm from "../../components/cart/CartForm";
import { useNavigate } from "react-router";
import userContext from "../../contexts/user-context";
import ProductionQuantityLimitsIcon from "@mui/icons-material/ProductionQuantityLimits";

const Checkout = () => {
  useDocumentTitle("Kitchen in Cloud | My Cart");

  const [openModal, setOpenModal] = useState(false);

  const [modalProps, setModalProps] = useState({
    id: null,
    title: "",
    priceId: null,
    image: "",
    price: null,
  });

  const navigate = useNavigate();

  const { isAuth } = useContext(authContext);

  const { cartItems, cartCount, deleteCart, delete_cart_loading, checkout } =
    useContext(cartContext);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
    if (!isAuth) {
      navigate("/login");
    }
  }, [isAuth]);

  const handleOnModalClosed = () => {
    setOpenModal(false);
  };

  const columns = [
    {
      field: "Image",

      width: 100,
      sortable: false,
      renderCell: (cellValues) => {
        return (
          <img className="CHECKOUT-IMG" src={cellValues.value} alt={"cart"} />
        );
      },
    },
    {
      field: "Product",
      headerName: "Product",
      width: 200,
      renderCell: (cellValues) => {
        console.log(cellValues);
        return (
          <Box>
            <Typography variant="body2">{cellValues.value}</Typography>
            <Typography variant="subtitle2" color="GrayText">
              {Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "NRS",
              }).format(cellValues.row.PricePer)}
            </Typography>
          </Box>
        );
      },
    },

    {
      field: "Quantity",
      headerName: "Quantity",
      type: "number",
      renderCell: (cellValues) => {
        return (
          <Box width="100px">
            <Typography align="center" variant="body2">
              {cellValues.value}
            </Typography>
          </Box>
        );
      },
    },
    {
      field: "Total",
      headerName: "Total",
      renderCell: (cellValues) => {
        return (
          <Box component="div">
            <Typography variant="subtitle2" color="info.main">
              {Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "NRS",
              }).format(cellValues.value)}
            </Typography>
          </Box>
        );
      },
    },
    {
      field: "Delete",
      renderCell: (cellValues) => {
        return (
          <IconButton
            disabled={delete_cart_loading}
            color="error"
            onClick={(event) => {
              handleDeleteClick(event, cellValues);
            }}
          >
            <DeleteIcon />
          </IconButton>
        );
      },
      sortable: false,
      filterable: false,
      width: 100,
    },
    {
      field: "Edit",
      renderCell: (cellValues) => {
        return (
          <Button
            color="success"
            onClick={(event) => {
              handleEditClick(event, cellValues);
            }}
          >
            edit
          </Button>
        );
      },
      sortable: false,
      filterable: false,
      width: 100,
    },
  ];

  let rows;

  if (cartItems.cartProducts !== undefined) {
    rows = cartItems.cartProducts.map((item) => {
      return {
        id: item.id.toString(),
        productId: item.product.id,
        priceId: item.product.unitPrice[0].id,
        Image: item.product.images[0].imageName,
        Product: item.product.title,
        PricePer: item.product.unitPrice[0].sellingPrice,
        Quantity: item.quantity,
        Total: item.price,
      };
    });
  }

  const handleDeleteClick = (e, value) => {
    deleteCart(parseInt(value.id));
  };
  const handleEditClick = (e, value) => {
    setModalProps({
      id: value.row.productId,
      title: value.row.Product,
      priceId: value.row.priceId,
      image: value.row.Image,
      price: value.row.PricePer,
    });
    setOpenModal(true);
  };

  return (
    <Box className="container" mb={5}>
      <Title text="My Cart" width="40px" />
      <Grid container spacing={1} justifyContent="space-between">
        <Grid item xs={12} lg={8}>
          <Box>
            <Typography variant="h5" pb={5}>
              My Shopping Cart:
              <Typography
                variant="h5"
                fontWeight="bold"
                color="info.main"
                display="inline"
              >
                &nbsp;{cartCount} item(s)
              </Typography>
            </Typography>
          </Box>

          <Box>
            {cartCount == 0 ? (
              <Box
                height="400px"
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
              >
                <ProductionQuantityLimitsIcon
                  sx={{ fontSize: "200px", color: "rgba(0,0,0,0.4)" }}
                />
                <Typography align="center" variant="h4" color="rgba(0,0,0,0.4)">
                  No items in cart
                </Typography>
              </Box>
            ) : (
              <div>
                <DataGrid
                  rows={rows}
                  columns={columns}
                  pageSize={5}
                  rowsPerPageOptions={[5]}
                  sortingOrder={["desc", "asc"]}
                  rowHeight={120}
                  //checkboxSelection
                  disableColumnMenu
                  disableSelectionOnClick
                  autoHeight
                  autoPageSize

                  // onSelectionModelChange={(ids) => {
                  //   const selectedIDs = new Set(ids);
                  //   const selected = rows.filter((row) =>
                  //     selectedIDs.has(row.id.toString())
                  //   );

                  //   setSelectedRowData(selected);
                  // }}
                  // selectionModel={selectedRowData.map((item) => item.id)}
                />
              </div>
            )}
          </Box>
        </Grid>
        <Grid item xs={12} lg={4}>
          <Box>
            {cartItems.cartProducts !== undefined && (
              <OrderSummary
                cartItems={cartItems}
                cartCount={cartCount}
                checkout={checkout}
              />
            )}
          </Box>
        </Grid>
      </Grid>

      <Modal open={openModal} onClose={handleOnModalClosed}>
        <Box className="MODAL-position">
          <CartForm
            id={modalProps.id}
            title={modalProps.title}
            priceId={modalProps.priceId}
            image={modalProps.image}
            price={modalProps.price}
            setOpenModal={setOpenModal}
          />
        </Box>
      </Modal>
    </Box>
  );
};

export default Checkout;

const OrderSummary = ({ cartItems, cartCount, checkout }) => {
  const {
    fetchDeliveryAddresses,
    deliveryAddresses,
    fetchPaymentMethod,
    paymentMethods,
  } = useContext(userContext);

  const [address, setAddress] = useState(null);
  const [paymentMethod, setPaymentMethod] = useState(null);

  const navigate = useNavigate();

  useEffect(() => {
    fetchDeliveryAddresses();
    fetchPaymentMethod();
  }, []);

  const handleOnCheckout = (e) => {
    e.preventDefault();
    if (address !== null && paymentMethod !== null) {
      checkout({ DeliveryId: address, PaymentMethodId: paymentMethod });
    }
  };

  return (
    <Box className="ORDER-SUMMARY-container">
      <Box className="ORDER-SUMMARY-title-container">
        <Typography variant="h6">Order Summary</Typography>
      </Box>
      <Box border className="ORDER-SUMMARY-item-container">
        <Box display="flex" justifyContent="space-between" p={2}>
          <Typography variant="body2" color="GrayText">
            Subtotal ({cartItems.cartProducts.length} products)
          </Typography>
          <Typography variant="body2" fontWeight="bold" color="GrayText">
            {Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "NRS",
            }).format(cartItems.subTotal)}
          </Typography>
        </Box>
        <Box display="flex" justifyContent="space-between" p={2}>
          <Typography variant="body2" color="GrayText">
            Shipping Fee
          </Typography>
          <Typography variant="body2" fontWeight="bold" color="GrayText">
            {Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "NRS",
            }).format(cartItems.deliveryCharge)}
          </Typography>
        </Box>
        <Box display="flex" justifyContent="space-between" p={2}>
          <Typography variant="h6" fontWeight="bold">
            Total
          </Typography>
          <Typography variant="h6" fontWeight="bold">
            {Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "NRS",
            }).format(parseFloat(cartItems.total))}
          </Typography>
        </Box>
        <Divider />
        <form onSubmit={handleOnCheckout}>
          <Box
            display="flex"
            alignItems="center"
            width="100%"
            sx={{ boxSizing: "border-box" }}
            p={2}
          >
            <Typography color="secondary" pr={1}>
              Delivery Address:
            </Typography>
            {deliveryAddresses !== undefined && deliveryAddresses.length > 0 ? (
              <FormControl fullWidth sx={{ minWidth: 180 }}>
                <InputLabel htmlFor="address-select">
                  Delivery Address
                </InputLabel>
                <Select
                  id="address-selec"
                  value={address}
                  onChange={(e) => {
                    setAddress(e.target.value);
                  }}
                >
                  {deliveryAddresses.map((item) => {
                    return (
                      <MenuItem key={item.id} value={item.id}>
                        {item.detail.formatted_address}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            ) : (
              <Box sx={{ border: "1px solid #e0e0e0" }} p={2}>
                <Typography color="GrayText">
                  You have not added delivery addresses yet.
                </Typography>
                <Button
                  onClick={() => {
                    navigate("/user");
                  }}
                >
                  Go to Profile
                </Button>
              </Box>
            )}
          </Box>
          <Box
            display="flex"
            alignItems="center"
            width="100%"
            sx={{ boxSizing: "border-box" }}
            p={2}
          >
            <Typography color="secondary" pr={1}>
              Payment Method:
            </Typography>
            {paymentMethods !== undefined && paymentMethods.length > 0 ? (
              <FormControl fullWidth sx={{ minWidth: 180 }}>
                <InputLabel htmlFor="payment-select">
                  Payment Methods
                </InputLabel>
                <Select
                  id="payment-select"
                  value={paymentMethod}
                  onChange={(e) => {
                    setPaymentMethod(e.target.value);
                  }}
                >
                  {paymentMethods.map((item) => {
                    return (
                      <MenuItem key={item.id} value={item.id}>
                        {item.title}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            ) : (
              <Box sx={{ border: "1px solid #e0e0e0" }} p={2}>
                <Typography color="GrayText">
                  Couldn't load Payment Method. Please check your connection and
                  try again.
                </Typography>
              </Box>
            )}
          </Box>
          <Divider />
          <Box display="flex" justifyContent="center" alignItems="center" p={2}>
            <Button type="submit" variant="contained" color="success">
              Checkout
            </Button>
          </Box>
        </form>
      </Box>
    </Box>
  );
};
