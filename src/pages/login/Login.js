import React from "react";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import Title from "../../ui/title/Title";
import { Box } from "@mui/material";
import "./login.css";
import LoginForm from "../../components/loginform/LoginForm";

const Login = () => {
  useDocumentTitle("Kitchen In Cloud | Login");

  return (
    <Box Box className="REGISTER-container">
      <Title text="login" width="30px" />
      <Box
        className="container"
        my={5}
        display="flex"
        sx={{ justifyContent: { xs: "center", md: "end" } }}
      >
        <LoginForm fromLogin={true} />
      </Box>
    </Box>
  );
};

export default Login;
