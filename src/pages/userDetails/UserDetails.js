import React, { useContext, useEffect, useState } from "react";
import authContext from "../../contexts/auth-context";
import { useNavigate } from "react-router";
import "./userdetails.css";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import {
  Avatar,
  Box,
  Button,
  Paper,
  Typography,
  Grid,
  Divider,
  Modal,
  FormControl,
  TextField,
  Checkbox,
  FormControlLabel,
  Skeleton,
} from "@mui/material";
import Title from "../../ui/title/Title";
import userContext from "../../contexts/user-context";
import LoadingButton from "@mui/lab/LoadingButton";

const UserDetails = () => {
  const [openModal, setOpenModal] = useState(false);
  const [updateClicked, setUpdateClicked] = useState(false);
  const [changePasswordClicked, setChangePasswordClicked] = useState(false);
  const [addressClicked, setAddressClicked] = useState(false);

  const { isAuth } = useContext(authContext);
  const {
    userData,
    deliveryAddresses,
    fetchDeliveryAddresses,
    removeDeliveryAddress,
  } = useContext(userContext);

  const navigate = useNavigate();
  useDocumentTitle("Kitchen in Cloud | User");

  useEffect(() => {
    if (!isAuth) {
      navigate("/login");
    }
  });

  useEffect(() => {
    fetchDeliveryAddresses();
    console.log("delivery address:", deliveryAddresses);
  }, []);

  const handleOnCloseModal = () => {
    setOpenModal(false);
    setUpdateClicked(false);
    setChangePasswordClicked(false);
    setAddressClicked(false);
  };

  return (
    <Box className="container" mb={5} minHeight="90vh">
      <Title text="My Profile" width="30px" />
      <Box>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6} lg={4}>
            <Paper align="center">
              <Box p={5}>
                <Avatar width="300px" height="300px" src={userData.image} />
                <Typography my={1}>
                  {userData.firstName === undefined ? (
                    <Skeleton width={100} />
                  ) : (
                    `${userData.firstName} ${userData.lastName}`
                  )}
                </Typography>
                <Typography mb={1}>
                  {userData.email === undefined ? (
                    <Skeleton width={200} />
                  ) : (
                    userData.email
                  )}
                </Typography>
                <Typography mb={3}>
                  {userData.mobileNumber === undefined ? (
                    <Skeleton width={100} />
                  ) : (
                    userData.mobileNumber
                  )}
                </Typography>
                <Box display="flex" justifyContent="space-between">
                  <Button
                    color="info"
                    variant="contained"
                    size="small"
                    onClick={() => {
                      setUpdateClicked(true);
                      setOpenModal(true);
                    }}
                  >
                    Update Profile
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => {
                      setChangePasswordClicked(true);
                      setOpenModal(true);
                    }}
                  >
                    Change Password
                  </Button>
                </Box>
              </Box>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6} lg={8}>
            <Paper>
              <Box p={5}>
                <Box p={1} display="flex" justifyContent="space-between">
                  <Typography mb={2} color="primary" fontWeight="bold">
                    Delivery Address
                  </Typography>
                  {deliveryAddresses.length < 3 && (
                    <Box>
                      <Button
                        variant="contained"
                        color="info"
                        size="small"
                        onClick={() => {
                          setAddressClicked(true);
                          setOpenModal(true);
                        }}
                      >
                        + Add Address
                      </Button>
                    </Box>
                  )}
                </Box>
                <Box>
                  {deliveryAddresses.length == 0 ? (
                    <Typography color="GrayText">
                      No delivery address yet.
                    </Typography>
                  ) : (
                    deliveryAddresses.map((item) => {
                      return (
                        <Box m={2}>
                          <Box display="flex" justifyContent="space-between">
                            <Typography>{item.title}</Typography>
                            <Button
                              onClick={() => {
                                removeDeliveryAddress(item.id);
                              }}
                            >
                              Remove
                            </Button>
                          </Box>
                          <Typography color="GrayText" fontStyle="italic">
                            {item.detail.formatted_address}
                          </Typography>

                          <Divider />
                        </Box>
                      );
                    })
                  )}
                </Box>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </Box>

      {/* modal */}
      <Modal open={openModal} onClose={handleOnCloseModal}>
        <Box className="MODAL-position">
          <Box p={2} display="flex" flexDirection="column" alignItems="center">
            {updateClicked && (
              <UpdateProfileForm handleOnCloseModal={handleOnCloseModal} />
            )}

            {changePasswordClicked && (
              <ChangePasswordForm handleOnCloseModal={handleOnCloseModal} />
            )}
            {addressClicked && (
              <AddAddressForm handleOnCloseModal={handleOnCloseModal} />
            )}
          </Box>
        </Box>
      </Modal>
    </Box>
  );
};

export default UserDetails;

const UpdateProfileForm = ({ handleOnCloseModal }) => {
  const { userData, updateUser } = useContext(userContext);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    updateUser({ "first-name": firstName, "last-name": lastName });
    setTimeout(() => {
      handleOnCloseModal();
    }, 1000);
  };

  useEffect(() => {
    setIsLoading(false);

    setFirstName(userData.firstName);
    setLastName(userData.lastName);
  }, []);
  return (
    <Box>
      <Typography align="center" my={3}>
        Update Profile
      </Typography>
      <form
        noValidate
        autoComplete="off"
        action="submit"
        onSubmit={handleSubmit}
      >
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="firstName"
            label="First Name"
            type="text"
            placeholder="Enter First Name"
            required
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value.trim());
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="lastName"
            label="Last Name"
            type="text"
            placeholder="Enter Last Name"
            required
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value.trim());
            }}
          />
        </FormControl>
        <Box display="flex" justifyContent="space-between">
          <LoadingButton
            loading={isLoading}
            type="submit"
            variant="contained"
            color="info"
            sx={{ margin: "10px" }}
          >
            Update
          </LoadingButton>
          <Button
            variant="contained"
            sx={{ margin: "10px" }}
            onClick={handleOnCloseModal}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Box>
  );
};

const ChangePasswordForm = ({ handleOnCloseModal }) => {
  const { changePassword, setChangePasswordSuccess, update_password_success } =
    useContext(userContext);

  const [oldPassword, setOldPasword] = useState("");
  const [newPassword, setNewPasword] = useState("");
  const [confirmPassword, setConfirmPasword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const { removeToken } = useContext(authContext);

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    changePassword({
      "new-password": newPassword,
      "old-password": oldPassword,
      "confirm-password": confirmPassword,
    });
  };

  useEffect(() => {
    setIsLoading(false);
    setChangePasswordSuccess(false);
  }, []);

  useEffect(() => {
    if (update_password_success === true) {
      setTimeout(() => {
        removeToken();
        handleOnCloseModal();
        navigate("/login");
      }, 1000);
    }
  }, [update_password_success]);

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Typography align="center" my={3}>
        Update Password
      </Typography>
      {update_password_success && (
        <Typography align="center" my={3}>
          Password updated successfuly
        </Typography>
      )}

      <form
        noValidate
        autoComplete="off"
        action="submit"
        onSubmit={handleSubmit}
      >
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="oldPassword"
            label="Old Password"
            type="password"
            placeholder="Enter Old Password"
            required
            value={oldPassword}
            onChange={(e) => {
              setOldPasword(e.target.value);
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="newPassword"
            label="New Password"
            type="password"
            placeholder="Enter New Password"
            required
            value={newPassword}
            onChange={(e) => {
              setNewPasword(e.target.value);
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="confirmPassword"
            label="Confirm Password"
            type="password"
            placeholder="Confirm Password"
            required
            value={confirmPassword}
            onChange={(e) => {
              setConfirmPasword(e.target.value);
            }}
          />
        </FormControl>
        <Box display="flex" justifyContent="space-between">
          <LoadingButton
            loading={isLoading}
            type="submit"
            variant="contained"
            color="info"
            sx={{ margin: "10px" }}
          >
            Update Password
          </LoadingButton>
          <Button
            variant="contained"
            sx={{ margin: "10px" }}
            onClick={handleOnCloseModal}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Box>
  );
};

const AddAddressForm = ({ handleOnCloseModal }) => {
  const { addDeliveryAddress } = useContext(userContext);

  const [title, setTitle] = useState("");
  const [latitute, setLatitute] = useState("");
  const [longitude, setLongitude] = useState("");
  const [isDefault, setIsDefault] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(false);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    addDeliveryAddress({
      title: title,
      latitude: latitute,
      longitude: longitude,
      isDefault: isDefault,
    });
    setTimeout(() => {
      handleOnCloseModal();
    }, 1000);
  };

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Typography align="center" my={3}>
        Add Address
      </Typography>

      <form
        noValidate
        autoComplete="off"
        action="submit"
        onSubmit={handleSubmit}
      >
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="title"
            label="Title"
            type="text"
            placeholder="Enter Title (Eg: Office)"
            required
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="latitude"
            label="Latitude"
            type="text"
            placeholder="Enter Latitude"
            required
            value={latitute}
            onChange={(e) => {
              setLatitute(e.target.value);
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="longitude"
            label="Longitude"
            type="text"
            placeholder="Enter Longitude"
            required
            value={longitude}
            onChange={(e) => {
              setLongitude(e.target.value);
            }}
          />
        </FormControl>
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <FormControlLabel
            control={
              <Checkbox
                checked={isDefault}
                onChange={() => {
                  setIsDefault(!isDefault);
                }}
              />
            }
            label="Set default"
          />
        </FormControl>
        <Box display="flex" justifyContent="space-between">
          <LoadingButton
            loading={isLoading}
            type="submit"
            variant="contained"
            color="info"
            sx={{ margin: "10px" }}
          >
            Add Address
          </LoadingButton>
          <Button
            variant="contained"
            sx={{ margin: "10px" }}
            onClick={handleOnCloseModal}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Box>
  );
};
