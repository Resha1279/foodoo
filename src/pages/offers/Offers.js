import React, { useContext, useEffect } from "react";
import ProductCard from "../../components/products/ProductCard";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { BASE_URL } from "../../constants/constants";
import Title from "../../ui/title/Title";
import Placeholder from "../../ui/placeholder/Placeholder";
import productContext from "../../contexts/product-context";
import { Grid, Skeleton } from "@mui/material";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import PlaceholderSlider from "../../ui/placeholder/PlaceholderSlider";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`home-tab-${index}`}
      aria-labelledby={`home-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `home-tab-${index}`,
    "aria-controls": `home-tab-${index}`,
  };
}

const Offers = ({ data, loading }) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let slideSettings = {
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 4,
    initialSlide: 0,
    lazyLoad: true,
    centerMode: true,

    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 4.5,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 1536,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3,
        },
      },

      {
        breakpoint: 1350,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 1050,
        settings: {
          slidesToShow: 2.5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1.5,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 610,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  let array = [1, 2, 3, 4, 5, 6];
  return (
    <div className="container">
      <Box>
        <Box
          sx={{
            marginBottom: "20px",
          }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="home-tab"
            centered
          >
            <Tab
              label={
                data[6] === undefined ? (
                  <Skeleton type="text" width="100px" />
                ) : (
                  data[6].sectionDetails.title
                )
              }
              sx={{
                fontWeight: "bold",
                fontSize: { xs: "0.8em", md: "1em", lg: "1.2em" },
                color: "common.white",
              }}
              {...a11yProps(0)}
            />
            <Tab
              label={
                data[2] === undefined ? (
                  <Skeleton type="text" width="100px" />
                ) : (
                  data[2].sectionDetails.title
                )
              }
              sx={{
                fontWeight: "bold",
                fontSize: { xs: "0.8em", md: "1em", lg: "1.2em" },
                color: "common.white",
              }}
              {...a11yProps(1)}
            />
            <Tab
              label={
                data[4] === undefined ? (
                  <Skeleton type="text" width="100px" />
                ) : (
                  data[4].sectionDetails.title
                )
              }
              sx={{
                fontWeight: "bold",
                fontSize: { xs: "0.8em", md: "1em", lg: "1.2em" },
                color: "common.white",
              }}
              {...a11yProps(2)}
            />
          </Tabs>
        </Box>

        <TabPanel value={value} index={0}>
          <Box>
            <Slider {...slideSettings}>
              {loading || data[6] === undefined
                ? array.map((item) => {
                    return <PlaceholderSlider key={item} />;
                  })
                : data[6].sectionDetails.products.map((item) => {
                    return (
                      <Box key={item.id}>
                        <ProductCard {...item} />
                      </Box>
                    );
                  })}
            </Slider>
          </Box>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <Box>
            <Slider {...slideSettings}>
              {loading || data[2] === undefined
                ? array.map((item) => {
                    return <PlaceholderSlider key={item} />;
                  })
                : data[2].sectionDetails.products.map((item) => {
                    return (
                      <Box key={item.id}>
                        <ProductCard {...item} />
                      </Box>
                    );
                  })}
            </Slider>
          </Box>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Box>
            <Slider {...slideSettings}>
              {loading || data[4] === undefined
                ? array.map((item) => {
                    return <PlaceholderSlider key={item} />;
                  })
                : data[4].sectionDetails.products.map((item) => {
                    return (
                      <Box key={item.id}>
                        <ProductCard {...item} />
                      </Box>
                    );
                  })}
            </Slider>
          </Box>
        </TabPanel>
      </Box>
    </div>
  );
};

export default Offers;
