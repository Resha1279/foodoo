import React from "react";
import { Grid } from "@mui/material";
import ProductsList from "../../components/products/ProductsList";
import CategoriesSidebar from "../../navigation/categories/CategoriesSidebar";

const Products = () => {
  return (
    <Grid container justifyContent="center" spacing={0} pb={5}>
      <Grid item lg={2} sx={{ display: { xs: "none", lg: "block" } }}>
        <CategoriesSidebar />
      </Grid>
      <Grid item lg={10} sm={12}>
        <ProductsList />
      </Grid>
    </Grid>
  );
};

export default Products;
