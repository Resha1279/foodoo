import React from "react";
import {
  Box,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import Title from "../../ui/title/Title";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import useDocumentTitle from "../../hooks/useDocumentTitle";

const Faq = () => {
  useDocumentTitle("FAQ's");
  return (
    <Box>
      <Title text="frequently asked questions(FAQ)" />
      <Box className="container" mb={5}>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>1. Q: How can I order</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: You can order easily using our online platform. When you find a
              product you need, you can add it to cart, login and go through the
              ordering process. After the order is ready, you will receive order
              summary to your email. Order summary will also be stored to your
              account. You can also easily make reorders afterwards by clicking
              the “reorder” button on any of your previously made orders. After
              clicking the “reorder” button the cart will open and you can
              change quantities or products.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>2. Q: Why should I buy online?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: Speeding up the process. By ordering online you will you will
              get prices faster and you will be able to go through order
              confirmation and payment process much faster. This could save days
              of your time. Traceability: You will have easy access to all of
              your previous orders any time you want. Reordering: you can make a
              re-order anytime based on your previous orders by only couple of
              clicks. This will save time and effort as you don’t need to go
              through all the documents and emails from the past.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>
              3. Q: What information should I input when ordering?
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: our online ordering system will ask for all the important
              information you should submit. If you have a VAT number, please
              remember to submit it. This will make sure the shipment is not
              delayed because of the lack of VAT number
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>4. Q: What payment methods can I use?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: You can use all the major credit cards. If you are a customer
              with established customer relationship with HyTest Ltd. you are
              able to use invoice as a payment method on our online shopping
              checkout process. If invoicing option is not activated for you
              although you are repeated customer, please contact us.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>5. Q: How can I change delivery address?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: Sign in to your account and go to “my account”. On “my account”
              you can change all your contact information.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>6. Q: What are the delivery charges?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: Delivery charges are dependent on the shipment requirements. If
              the products on your order are due to special requirements (for
              example dry ice) extra fee will be added to the shipment charges.
              You can see the shipping fees on the checkout process before the
              payment is made.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography>7. Q: Can I cancel my order?</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              A: If you want to cancel your order, please do so as soon as
              possible. If we have already processed your order, you need to
              contact us and return the product.
            </Typography>
          </AccordionDetails>
        </Accordion>
      </Box>
    </Box>
  );
};

export default Faq;
