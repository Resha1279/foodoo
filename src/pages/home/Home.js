import Box from "@mui/material/Box";
import React, { useContext, useRef } from "react";
import Title from "../../ui/title/Title";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import "./home.css";
import { Button, Grid, Skeleton, Typography } from "@mui/material";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import KeyboardReturnOutlinedIcon from "@mui/icons-material/KeyboardReturnOutlined";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import SupportAgentOutlinedIcon from "@mui/icons-material/SupportAgentOutlined";
import productContext from "../../contexts/product-context";
import { Link } from "react-router-dom";
import useFetch from "../../hooks/useFetch";
import { BASE_URL, API_KEY } from "../../constants/constants";
import Offers from "../offers/Offers";
import { useNavigate } from "react-router-dom";
import PlaceholderCategoriesHome from "../../ui/placeholder/PlaceholderCategoriesHome";

const Home = () => {
  useDocumentTitle("Kitchen in Cloud | Home");
  const navigate = useNavigate();

  let scrollCategoriesRef = useRef();

  const { categories, categories_loading, setCategory } =
    useContext(productContext);

  const { data, loading, error } = useFetch(
    `${BASE_URL}/api/v4/newhome`,

    {
      method: "GET",
      headers: {
        "Warehouse-Id": "1",
        "Api-key": API_KEY,
        "Content-Type": "application/json",
      },
      redirect: "follow",
    }
  );

  return (
    <>
      {/* landing section */}
      <Box
        className="HOME-landing"
        sx={{
          backgroundImage: "url(/assets/landing.jpg)",
        }}
      >
        <Box className=" HOME-landing-text-wrapper">
          <Box my={5} p={5} bgcolor="rgba(0, 0, 0, 0.568)">
            <Typography
              fontWeight="bold"
              color="common.white"
              fontSize="2.7em"
              align="center"
            >
              <Typography fontSize="0.8em" display="inline" color="primary">
                Welcome to
              </Typography>{" "}
              Kitchen in cloud!
            </Typography>

            <Typography
              fontSize="1.2em"
              color="rgba(255, 255, 255, 0.668)"
              align="center"
            >
              It's Love at First Bite
            </Typography>
            <Box display="flex" justifyContent="center" pt={5}>
              <Button
                color="primary"
                variant="contained"
                size="large"
                sx={{
                  transition: "all 0.3s ease-in-out",
                  transform: "scale(1.3)",
                  ":hover": {
                    bgcolor: "primary.main",
                    transform: "scale(1.4)",
                  },
                }}
                onClick={() => {
                  window.scrollTo({
                    behavior: "smooth",
                    top: scrollCategoriesRef.current.offsetTop - 110,
                  });
                }}
              >
                Explore now!
              </Button>
            </Box>
          </Box>
        </Box>
        {/* delivery info section */}
        <Box className="HOME-delivery-info container">
          <Grid container justifyContent="center" spacing={3}>
            <Grid item container spacing={1} xs={6} md={3}>
              <Grid item>
                <LocalShippingOutlinedIcon fontSize="large" color="primary" />
              </Grid>
              <Grid item>
                <Typography fontWeight="bold" color="common.white">
                  Free Shipping
                </Typography>
                <Typography
                  variant="body2"
                  color="primary"
                  sx={{ width: "180px" }}
                >
                  On orders over NRS 1500
                </Typography>
              </Grid>
            </Grid>
            <Grid item container spacing={1} xs={6} md={3}>
              <Grid item>
                <KeyboardReturnOutlinedIcon fontSize="large" color="primary" />
              </Grid>
              <Grid item>
                <Typography fontWeight="bold" color="common.white">
                  Free Returns
                </Typography>
                <Typography
                  color="primary"
                  sx={{ width: "180px" }}
                  variant="body2"
                >
                  Free returns within 1 hour of delivery
                </Typography>
              </Grid>
            </Grid>
            <Grid item container spacing={1} xs={6} md={3}>
              <Grid item>
                <LockOutlinedIcon fontSize="large" color="primary" />
              </Grid>
              <Grid item>
                <Typography fontWeight="bold" color="common.white">
                  100% Secure Payment
                </Typography>
                <Typography
                  color="primary"
                  sx={{ width: "180px" }}
                  variant="body2"
                >
                  Your payment details are safe with us
                </Typography>
              </Grid>
            </Grid>
            <Grid item container spacing={1} xs={6} md={3}>
              <Grid item>
                <SupportAgentOutlinedIcon fontSize="large" color="primary" />
              </Grid>
              <Grid item>
                <Typography fontWeight="bold" color="common.white">
                  Support 10 Am - 8 Pm
                </Typography>
                <Typography
                  color="primary"
                  sx={{ width: "180px" }}
                  variant="body2"
                >
                  Contact us from 10 am to 8 pm, every day
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>

      <Box className="HOME-categories-ads-wrapper">
        {/* categories section */}
        <Box ref={scrollCategoriesRef}>
          <Title text="Explore categories" />
          <Box className="container">
            <Grid container spacing={1} justifyContent="center">
              {categories_loading ? (
                <PlaceholderCategoriesHome />
              ) : (
                categories.map((category, index) => (
                  <Grid item key={index}>
                    <Box
                      bgcolor="white"
                      className="HOME-category-item"
                      onClick={() => {
                        setCategory(category);
                        window.scrollTo({
                          top: 0,
                          behavior: "smooth",
                        });
                      }}
                    >
                      <Link to="/products">
                        <img
                          className="HOME-category-image"
                          src={category.backgroundImage}
                          srcSet={category.backgroundImage}
                          alt={category.title}
                          loading="lazy"
                        />

                        <Box className="HOME-category-title">
                          <Typography
                            color="common.white"
                            sx={{ textTransform: "uppercase" }}
                          >
                            {category.title}
                          </Typography>
                          <Typography variant="body2" color="common.white">
                            {category.productCount} products
                          </Typography>
                        </Box>
                      </Link>
                    </Box>
                  </Grid>
                ))
              )}
            </Grid>
          </Box>
        </Box>

        {/* Ad banner section */}
        <Box className="container" mt={5}>
          {loading ? (
            <Box display="flex" justifyContent="space-between">
              <Box width="50%" height="240px" m={2}>
                <Skeleton variant="image" width="100%" height="100%" />
              </Box>
              <Box width="50%" height="240px" m={2}>
                <Skeleton variant="image" width="100%" height="100%" />
              </Box>
            </Box>
          ) : data[6] === undefined ? (
            <Box display="flex" justifyContent="space-between">
              <Box width="50%" height="240px" m={2}>
                <Skeleton width="100%" height="100%" />
              </Box>
              <Box width="50%" height="240px" m={2}>
                <Skeleton width="100%" height="100%" />
              </Box>
            </Box>
          ) : (
            <Box display="flex" justifyContent="space-between">
              <Box
                width="50%"
                height="240px"
                m={2}
                sx={{
                  cursor: "pointer",
                  overflow: "hidden",
                }}
                onClick={() => {
                  let category = categories.filter(
                    (category) => category.id == data[5].details[0].category
                  );
                  setCategory(category[0]);
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                  navigate("/products");
                }}
              >
                <img
                  className="HOME-Ads-image"
                  width="100%"
                  src={data[5].details[0].images}
                  alt="ad1"
                  loading="lazy"
                />
              </Box>
              <Box
                width="50%"
                height="240px"
                m={2}
                sx={{ cursor: "pointer", overflow: "hidden" }}
                onClick={() => {
                  let category = categories.filter(
                    (category) => category.id == data[5].details[1].category
                  );
                  setCategory(category[0]);
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                  navigate("/products");
                }}
              >
                <img
                  className="HOME-Ads-image"
                  width="100%"
                  src={data[5].details[1].images}
                  alt="ad1"
                  loading="lazy"
                />
              </Box>
            </Box>
          )}
        </Box>
      </Box>

      {/* hot deals section */}
      <Box
        className="HOME-hot-deals"
        sx={{ paddingTop: "100px", paddingBottom: "100px" }}
      >
        <Offers data={data} loading={loading} />
      </Box>
    </>
  );
};

export default Home;
