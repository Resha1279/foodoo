import { Grid, Paper, Box, Typography, Button, Modal } from "@mui/material";

import React, { useContext, useEffect, useState } from "react";
import productContext from "../../contexts/product-context";
import { useParams } from "react-router-dom";
import "./single-product.css";
import PlaceholderSingleProduct from "../../ui/placeholder/PlaceholderSingleProduct";
import cartContext from "../../contexts/cart-context";
import CustomAlert from "../../ui/alert/Alert";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import LoginForm from "../../components/loginform/LoginForm";
import authContext from "../../contexts/auth-context";
import CartForm from "../../components/cart/CartForm";

const SingleProduct = () => {
  const {
    singleProduct: { images, title, categoryTitle, unitPrice },
    product_loading,
    fetchSingleProduct,
  } = useContext(productContext);
  const { isAuth } = useContext(authContext);

  useDocumentTitle(`${title}`);

  const { id } = useParams();

  const ID = parseInt(id);

  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    fetchSingleProduct(ID);
  }, [ID]);

  useEffect(() => {
    setOpenModal(false);
  }, [isAuth]);

  const handleOnModalClosed = () => {
    setOpenModal(false);
  };

  return (
    <>
      {product_loading ? (
        <PlaceholderSingleProduct />
      ) : (
        <Box className="container" my={4}>
          <Grid container spacing={4}>
            <Grid item xs={12} sm={5}>
              <Paper>
                <img
                  src={
                    images
                      ? images[0].imageName
                      : "https://landerapp.com/blog/wp-content/uploads/2018/05/MAG-FR-Oestreicher-Singer-Product-Recommendation-Viral-Marketing-Social-Media-Network-Ecommerce-1200-1200x627.jpg"
                  }
                  alt={title}
                  className="SINGLE-PRODUCT-image"
                  onError={(e) => {
                    e.target.onerror = null;
                    e.target.src =
                      "https://landerapp.com/blog/wp-content/uploads/2018/05/MAG-FR-Oestreicher-Singer-Product-Recommendation-Viral-Marketing-Social-Media-Network-Ecommerce-1200-1200x627.jpg";
                  }}
                />
              </Paper>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Typography
                variant="button"
                sx={{ fontWeight: "bold", fontSize: "1.5em" }}
              >
                {title}
              </Typography>

              <Typography sx={{ fontWeight: "bold" }}>Category:</Typography>
              <Typography inline>{categoryTitle}</Typography>
              <Typography
                variant="h6"
                py={2}
                color="info.main"
                sx={{ fontWeight: "bold" }}
              >
                {unitPrice
                  ? Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "NRS",
                    }).format(unitPrice[0].markedPrice)
                  : 0}
              </Typography>
              <Button
                variant="contained"
                sx={{
                  ":hover": {
                    bgcolor: "info.main",
                    color: "white",
                  },
                  borderRadius: 0,
                }}
                onClick={() => {
                  setOpenModal(true);
                }}
              >
                Add to cart
              </Button>
            </Grid>
          </Grid>

          <Modal open={openModal} onClose={handleOnModalClosed}>
            <Box className="MODAL-position">
              {isAuth ? (
                <CartForm
                  id={ID}
                  title={title}
                  priceId={unitPrice ? unitPrice[0].id : null}
                  image={images ? images[0].imageName : null}
                  price={unitPrice ? unitPrice[0].markedPrice : 0}
                  setOpenModal={setOpenModal}
                />
              ) : (
                <Box
                  p={2}
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  bgcolor="info.main"
                >
                  <Typography variant="h6" align="center" color="common.white">
                    You are not logged in
                  </Typography>
                  <Typography
                    variant="overline"
                    color="rgb(255,255,255,0.8)"
                    aLign="center"
                  >
                    Please Login to continue
                  </Typography>
                  <Box>
                    <LoginForm setOpenModal={setOpenModal} />
                  </Box>
                </Box>
              )}
            </Box>
          </Modal>
        </Box>
      )}
    </>
  );
};

export default SingleProduct;
