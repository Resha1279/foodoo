import React from "react";
import { Typography, Box, Grid, Divider } from "@mui/material";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import MailIcon from "@mui/icons-material/Mail";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { Link } from "react-router-dom";
import "./footer.css";

const Footer = () => {
  return (
    <Box>
      <Box
        sx={{
          backgroundColor: "secondary.main",
          height: "500px",
          boxSizing: "border-box",
          display: "flex",
        }}
      >
        <Box
          className="FOOTER-logo"
          sx={{ display: { xs: "none", lg: "flex" } }}
        >
          <Link
            to="/"
            onClick={() => {
              window.scrollTo({
                top: 0,
                behavior: "smooth",
              });
            }}
          >
            <img
              className="TOPNAV-logo"
              src="/assets/kitchenincloudlogo.png"
              alt="logo"
            />
          </Link>
        </Box>
        <Box className="container ">
          <Grid container justifyContent="center" spacing={3}>
            <Grid item container spacing={1} xs={6} md={3}>
              <Typography color="common.white" py={4}>
                CONTACT
              </Typography>
              <Divider />
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <Box
                    sx={{ border: "1px solid", borderColor: "primary.main" }}
                    p={0.5}
                  >
                    <LocationOnIcon color="primary" />
                  </Box>
                </Grid>
                <Grid item>
                  <a
                    href="https://www.google.com/maps/place/E.K.+Solutions+Pvt+Ltd/@27.6851504,85.3201977,21z/data=!4m5!3m4!1s0x0:0x13da31f6765fce59!8m2!3d27.6851463!4d85.3203125"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                      py={1}
                    >
                      1234k Avenue, 4th block, New York City.
                    </Typography>
                  </a>
                </Grid>
              </Grid>

              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <Box
                    sx={{ border: "1px solid", borderColor: "primary.main" }}
                    p={0.5}
                  >
                    <MailIcon color="primary" />
                  </Box>
                </Grid>
                <Grid item>
                  <a href="mailto:info@example.com?subject = Contact&body = Message">
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                      py={1}
                    >
                      info@example.com
                    </Typography>
                  </a>
                </Grid>
              </Grid>

              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <Box
                    sx={{ border: "1px solid", borderColor: "primary.main" }}
                    p={0.5}
                  >
                    <LocalPhoneIcon color="primary" />
                  </Box>
                </Grid>
                <Grid item>
                  <Typography color={"rgb(255,255,255,0.6)"} py={1}>
                    +1234 567 567
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item container spacing={1} xs={6} md={3}>
              <Typography color="common.white" py={3}>
                INFORMATION
              </Typography>
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/about"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                      py={1}
                    >
                      About Us
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/contact"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                      py={1}
                    >
                      Contact Us
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/faq"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                      py={1}
                    >
                      FAQ's
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            <Grid item container xs={6} md={3} spacing={1}>
              <Typography color="common.white" py={3}>
                PROFILE
              </Typography>

              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/checkout"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      py={1}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                    >
                      My Cart
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/login"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      py={1}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                    >
                      Login
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
              <Grid
                container
                direction="row"
                wrap="nowrap"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <ArrowForwardIcon color="primary" />
                </Grid>
                <Grid item>
                  <Link
                    to="/register"
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  >
                    <Typography
                      color={"rgb(255,255,255,0.6)"}
                      py={1}
                      sx={{
                        "&:hover": {
                          color: "primary.main",
                        },
                      }}
                    >
                      Create Account
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Box component="div" textAlign="right" p={3} bgcolor="info.main">
        <Typography
          variant="caption"
          color={"rgb(255,255,255,0.6)"}
          mt={5}
          gutterBottom
        >
          © {new Date().getFullYear()} Super Market. All rights reserved |
          Design by&nbsp;
          <Typography variant="caption" fontWeight="bold" color="common.white">
            Resha Adhikari&nbsp;
          </Typography>
        </Typography>
      </Box>
    </Box>
  );
};

export default Footer;
