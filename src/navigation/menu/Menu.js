import React, { useContext, useState, useEffect } from "react";
import AppBar from "@mui/material/AppBar";
import {
  Button,
  Modal,
  IconButton,
  Box,
  Grid,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import CategoriesDropdown from "../categories/CategoriesDropdown";
import CategoriesNavList from "../categories/CategoriesNavList";
import Badge from "@mui/material/Badge";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";

import cartContext from "../../contexts/cart-context";
import "./menu.css";
import authContext from "../../contexts/auth-context";
import LoginForm from "../../components/loginform/LoginForm";
import { useNavigate } from "react-router";

const Menu = () => {
  const [openCart, setOpenCart] = useState(false);

  const { isAuth, setToken } = useContext(authContext);
  const { cartCount, fetchCart, cartItems } = useContext(cartContext);
  const navigate = useNavigate();

  useEffect(() => {
    const token = JSON.parse(window.localStorage.getItem("token"));
    if (token) {
      setToken(token);
    }
  }, [isAuth]);

  useEffect(() => {
    if (isAuth) {
      fetchCart();
    }
  }, [isAuth]);

  const handleOnCartClicked = () => {
    if (isAuth) {
      navigate("/checkout");
    } else {
      setOpenCart(!openCart);
    }
  };

  return (
    <AppBar position="static" color="primary" elevation={0}>
      <Box className="container">
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Grid item container alignItems="center">
            <Link
              to="/"
              onClick={() => {
                window.scrollTo({
                  top: 0,
                  behavior: "smooth",
                });
              }}
            >
              <Button
                color="secondary"
                sx={{
                  fontWeight: "bold",
                  color: "black",
                  "&:hover": {
                    transform: "scale(1.05)",
                    transition: "0.3s ease",
                  },
                }}
              >
                Home
              </Button>
            </Link>
            <Box sx={{ display: { xs: "block", lg: "none" } }}>
              <CategoriesDropdown />
            </Box>
            <Box sx={{ display: { xs: "none", lg: "block" } }}>
              <CategoriesNavList />
            </Box>
          </Grid>
          <Grid item>
            <div className="cart-icon">
              <IconButton
                size="large"
                aria-label="notifications"
                color="secondary"
                sx={{
                  "&:hover": {
                    color: "black",
                  },
                }}
                onClick={handleOnCartClicked}
              >
                <Badge
                  badgeContent={isAuth ? cartCount : 0}
                  color="info"
                  showZero
                >
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </div>
          </Grid>
        </Box>
      </Box>
      <Modal open={openCart} onClose={handleOnCartClicked}>
        <Box className="MODAL-position">
          <Box
            p={2}
            display="flex"
            flexDirection="column"
            justifyContent="center"
            bgcolor="info.main"
          >
            <Typography variant="h6" align="center" color="common.white">
              You are not logged in
            </Typography>
            <Typography
              variant="overline"
              color="rgb(255,255,255,0.8)"
              aLign="center"
            >
              Please Login to continue
            </Typography>
            <Box>
              <LoginForm setOpenModal={handleOnCartClicked} />
            </Box>
          </Box>
        </Box>
      </Modal>
    </AppBar>
  );
};

export default Menu;
