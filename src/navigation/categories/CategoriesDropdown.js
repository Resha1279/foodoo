import React, { useState, useRef, useContext, useEffect } from "react";
import {
  Button,
  Paper,
  Popper,
  ClickAwayListener,
  Grow,
  MenuItem,
  MenuList,
} from "@mui/material";
import { Link } from "react-router-dom";
import { KeyboardArrowDown } from "@mui/icons-material";
import productContext from "../../contexts/product-context";
import PlaceholderCategories from "../../ui/placeholder/PlaceholderCategories";

const CategoriesDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { setCategory, fetchCategories, categories, categories_loading } =
    useContext(productContext);
  const anchorRef = useRef(null);

  useEffect(() => {
    fetchCategories();
  }, []);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };
  const handleClose = (event, category) => {
    if (category) {
      setCategory(category);
    }
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setIsOpen(false);
  };
  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setIsOpen(false);
    } else if (event.key === "Escape") {
      setIsOpen(false);
    }
  }
  const prevOpen = useRef(isOpen);
  React.useEffect(() => {
    if (prevOpen.current === true && isOpen === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = isOpen;
  }, [isOpen]);
  return (
    <>
      <Button
        ref={anchorRef}
        id="categories-dropdown"
        aria-controls={isOpen ? "categories-menu" : undefined}
        aria-expanded={isOpen ? "true" : undefined}
        aria-haspopup="true"
        color="secondary"
        sx={{
          fontWeight: "bold",
          color: "black",
          "&:hover": {
            transform: "scale(1.05)",
            transition: "0.3s ease",
          },
        }}
        onClick={(e) => {
          handleClick();
        }}
        endIcon={<KeyboardArrowDown />}
      >
        Categories
      </Button>

      <Popper
        open={isOpen}
        anchorEl={anchorRef.current}
        role={undefined}
        placement="bottom-start"
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom-start" ? "left top" : "left bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                {categories_loading ? (
                  <PlaceholderCategories />
                ) : (
                  <MenuList
                    autoFocusItem={isOpen}
                    id="category-menu"
                    aria-labelledby="category-button"
                    onKeyDown={handleListKeyDown}
                  >
                    <Link
                      to="/products"
                      onClick={() => {
                        window.scrollTo({
                          top: 0,
                          behavior: "smooth",
                        });
                      }}
                    >
                      <MenuItem
                        onClick={(e) => {
                          handleClose(e, "all");
                        }}
                      >
                        <Button color="secondary" sx={{ fontWeight: "bold" }}>
                          All Items
                        </Button>
                      </MenuItem>
                    </Link>
                    {categories.map((category, index) => {
                      return (
                        <Link
                          to="/products"
                          key={index}
                          onClick={() => {
                            window.scrollTo({
                              top: 0,
                              behavior: "smooth",
                            });
                          }}
                        >
                          <MenuItem
                            onClick={(e) => {
                              handleClose(e, category);
                            }}
                          >
                            <Button
                              color="secondary"
                              sx={{ fontWeight: "bold" }}
                            >
                              {category.title}
                            </Button>
                          </MenuItem>
                        </Link>
                      );
                    })}
                  </MenuList>
                )}
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};

export default CategoriesDropdown;
