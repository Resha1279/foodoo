import React, { useContext } from "react";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import productContext from "../../contexts/product-context";
import { Drawer, Box, Toolbar, Typography } from "@mui/material";
import "./categories.css";
import PlaceholderCategories from "../../ui/placeholder/PlaceholderCategories";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

const CategoriesSidebar = () => {
  const {
    categories,
    setCategory,
    categories_loading,
    activeCategory,
    search,
  } = useContext(productContext);

  const handleClick = (category) => {
    if (category) {
      setCategory(category);
    }
  };
  return (
    <div className="CATEGORIES-SIDEBAR-drawer">
      <Drawer
        variant="permanent"
        sx={{
          backgroundColor: "secondary.main",
          width: "250px",
          flexShrink: 0,
          overflow: "hidden",
          [`& .MuiDrawer-paper`]: {
            width: "250px",
            boxSizing: "border-box",
          },
          display: { xs: "none", lg: "block" },
        }}
      >
        <Box sx={{ mt: "126px", overflowX: "hidden" }}>
          <Typography
            p={2}
            variant="h6"
            color={"text.secondary"}
            sx={{ fontWeight: "bold", textTransform: "uppercase" }}
          >
            Categories
          </Typography>

          {categories_loading ? (
            <PlaceholderCategories />
          ) : (
            <List>
              <ListItem
                button
                onClick={() => {
                  handleClick("all");
                }}
                sx={{
                  transition: "all 0.3s ease-in-out",
                  ":hover": {
                    transform: "translate(5px)",
                    color: "red",
                  },
                }}
              >
                <ListItemText
                  primary="ALL ITEMS"
                  sx={
                    activeCategory.title || search
                      ? {
                          color: "GrayText",
                          ":hover": {
                            color: "red",
                          },
                        }
                      : { color: "red" }
                  }
                />
              </ListItem>
              {categories.map((category, index) => {
                return (
                  <div key={index}>
                    {category.subcategories.length < 1 ? (
                      <div>
                        <ListItem
                          button
                          onClick={() => {
                            handleClick(category);
                          }}
                          sx={{
                            transition: "all 0.3s ease-in-out",

                            ":hover": {
                              transform: "translate(5px)",
                              color: "red",
                            },
                          }}
                        >
                          <ListItemText
                            sx={
                              category.title === activeCategory.title
                                ? {
                                    color: "red",
                                  }
                                : {
                                    color: "GrayText",
                                    ":hover": {
                                      color: "red",
                                    },
                                  }
                            }
                            primary={category.title}
                          />
                        </ListItem>
                      </div>
                    ) : (
                      <div>
                        <ListItem
                          button
                          onClick={() => {
                            handleClick(category);
                          }}
                          sx={{
                            transition: "all 0.3s ease-in-out",
                            ":hover": {
                              transform: "translate(5px)",
                              color: "red",
                            },
                          }}
                        >
                          <ListItemText
                            className="CATEGORIES-SIDEBAR-list-item"
                            sx={
                              category.title === activeCategory.title
                                ? {
                                    color: "red",
                                  }
                                : {
                                    color: "GrayText",
                                    ":hover": {
                                      color: "red",
                                    },
                                  }
                            }
                            primary={category.title}
                          />
                        </ListItem>
                        {category.subcategories.map((sub, index) => {
                          return (
                            <div key={index}>
                              <ListItem
                                button
                                onClick={() => {
                                  handleClick(sub);
                                }}
                                sx={{
                                  transition: "all 0.3s ease-in-out",
                                  ":hover": {
                                    transform: "translate(5px)",
                                    color: "red",
                                  },
                                }}
                              >
                                <ArrowForwardIcon
                                  fontSize="small"
                                  color="disabled"
                                />
                                <ListItemText
                                  className="CATEGORIES-SIDEBAR-list-item"
                                  sx={
                                    sub.title === activeCategory.title
                                      ? {
                                          color: "red",
                                        }
                                      : {
                                          color: "GrayText",
                                          ":hover": {
                                            color: "red",
                                          },
                                        }
                                  }
                                  primary={sub.title}
                                />
                              </ListItem>
                            </div>
                          );
                        })}
                      </div>
                    )}
                  </div>
                );
              })}
            </List>
          )}
        </Box>
      </Drawer>
    </div>
  );
};

export default CategoriesSidebar;
