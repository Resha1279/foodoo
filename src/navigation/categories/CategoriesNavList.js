import React, { useState, useContext, useEffect } from "react";
import {
  Button,
  MenuItem,
  MenuList,
  List,
  ListItem,
  Grid,
} from "@mui/material";
import { Link } from "react-router-dom";
import productContext from "../../contexts/product-context";
import PlaceholderCategoriesTopNav from "../../ui/placeholder/PlaceholderCategoriesTopNav";

const CategoriesNavList = () => {
  const { setCategory, fetchCategories, categories, categories_loading } =
    useContext(productContext);

  useEffect(() => {
    fetchCategories();
  }, []);

  const handleClose = (event, category) => {
    if (category) {
      console.log(category);
      setCategory(category);
    }
  };

  return (
    <div>
      {categories_loading ? (
        <PlaceholderCategoriesTopNav />
      ) : (
        <Grid container>
          <Link
            to="/products"
            onClick={() => {
              window.scrollTo({
                top: 0,
                behavior: "smooth",
              });
            }}
          >
            <Button
              color="secondary"
              onClick={(e) => {
                handleClose(e, "all");
              }}
              sx={{
                fontWeight: "bold",
                color: "black",
                "&:hover": {
                  transform: "scale(1.05)",
                  transition: "0.3s ease",
                },
              }}
            >
              All Items
            </Button>
          </Link>
          {categories.map((category, index) => {
            return (
              <Link
                to="/products"
                key={index}
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Button
                  color="secondary"
                  onClick={(e) => {
                    handleClose(e, category);
                  }}
                  sx={{
                    fontWeight: "bold",
                    color: "black",
                    "&:hover": {
                      transform: "scale(1.05)",
                      transition: "0.3s ease",
                    },
                  }}
                >
                  {category.title}
                </Button>
              </Link>
            );
          })}
        </Grid>
      )}
    </div>
  );
};

export default CategoriesNavList;
