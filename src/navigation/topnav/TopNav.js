import React, { useEffect, useState, useContext } from "react";
import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import { Box, Grid, Button, Avatar, Skeleton, Paper } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { BASE_URL, API_KEY } from "../../constants/constants";

import SearchIcon from "@mui/icons-material/Search";
import "./topnav.css";
import productContext from "../../contexts/product-context";
import authContext from "../../contexts/auth-context";
import userContext from "../../contexts/user-context";
import ClickAwayListener from "@mui/material/ClickAwayListener";

const TopNav = () => {
  const [searchInput, setSearchInput] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);

  const { setSearchText } = useContext(productContext);
  const { isAuth, removeToken } = useContext(authContext);
  const { userData, setUserData } = useContext(userContext);

  const navigate = useNavigate();

  useEffect(() => {
    if (searchInput === "") {
      setShowSuggestions(false);
    } else {
      let header = {
        "Warehouse-Id": "1",
        "Api-key": API_KEY,
        "Content-Type": "application/json",
      };
      fetch(`${BASE_URL}/api/v4/suggest?query=${searchInput}`, {
        method: "GET",
        headers: header,
        redirect: "follow",
      })
        .then((response) => {
          console.log("response status: ", response.status);

          if (!response.ok) {
            throw new Error("HTTP status " + response.status);
          }
          return response.json();
        })
        .then((result) => {
          console.log("suggestions Success:", result.data);
          setSuggestions(result.data);
          if (result.data.length > 0 && searchInput !== "") {
            setShowSuggestions(true);
          } else {
            setShowSuggestions(false);
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  }, [searchInput]);

  useEffect(() => {
    if (isAuth) {
      setUserData();
    }
  }, [isAuth]);

  const handleOnSearchButtonClicked = (e) => {
    setShowSuggestions(false);
    e.preventDefault();
    if (searchInput.trim() !== "") {
      setSearchText(searchInput.trim());
      setSearchInput("");

      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
      navigate("/products");
    }
  };

  return (
    <AppBar position="static" color="secondary" p={0} elevation={0}>
      <Box className="container ">
        <Grid container alignItems="center">
          {/* logo */}
          <Grid item xs={6} md={4}>
            <Link
              to="/"
              onClick={() => {
                window.scrollTo({
                  top: 0,
                  behavior: "smooth",
                });
              }}
            >
              <img
                className="TOPNAV-logo"
                src="/assets/kitchenincloudlogo.png"
                alt="logo"
              />
            </Link>
          </Grid>

          {/* login | register for smaller screen */}
          <Grid
            item
            xs={6}
            md={4}
            container
            justifyContent="flex-end"
            alignItems="center"
            sx={{ display: { xs: "flex", md: "none" } }}
          >
            {isAuth ? (
              <Link
                to="/user"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Box display="flex" alignItems="center">
                  <Avatar src={userData.image} sx={{ width: 24, height: 24 }} />
                  <Typography variant="subtitle2" color="common.white" pl={1}>
                    {userData.firstName === undefined ? (
                      <Skeleton width={100} />
                    ) : (
                      `${userData.firstName} ${userData.lastName}`
                    )}
                  </Typography>
                </Box>
              </Link>
            ) : (
              <Link
                to="/login"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Typography
                  variant="button"
                  color="primary.main"
                  sx={{
                    "&:hover": {
                      color: "common.white",
                    },
                  }}
                >
                  login
                </Typography>
              </Link>
            )}

            <Typography variant="button" color="rgba(255,255,255,0.5)">
              &nbsp; | &nbsp;
            </Typography>
            {isAuth ? (
              <Button
                color="primary"
                sx={{
                  "&:hover": {
                    color: "common.white",
                  },
                }}
                onClick={() => {
                  removeToken();
                }}
              >
                Logout
              </Button>
            ) : (
              <Link
                to="/register"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Typography
                  variant="button"
                  color="primary.main"
                  sx={{
                    "&:hover": {
                      color: "common.white",
                    },
                  }}
                >
                  Register
                </Typography>
              </Link>
            )}
          </Grid>

          {/* search */}
          <Grid
            item
            xs={12}
            md={4}
            sx={{
              marginBottom: { xs: "10px", md: "0px" },
            }}
            container
            justifyContent="center"
          >
            <form
              className="TOPNAV-search-form"
              onSubmit={handleOnSearchButtonClicked}
            >
              <Box
                component="div"
                className="TOPNAV-search-wrapper"
                display="flex"
              >
                <Box className="TOPNAV-search-input-wrapper">
                  <InputBase
                    className="TOPNAV-search-input"
                    placeholder="Search.."
                    type="search"
                    autocomplete="off"
                    value={searchInput}
                    onChange={(e) => {
                      setSearchInput(e.target.value);
                      e.target.value == "" && setShowSuggestions(false);
                    }}
                  />
                </Box>

                <Box
                  className="TOPNAV-search-icon-wrapper"
                  backgroundColor="primary.main"
                  px={1}
                >
                  <IconButton type="submit">
                    <SearchIcon />
                  </IconButton>
                </Box>

                {showSuggestions && (
                  <ClickAwayListener
                    onClickAway={() => {
                      setShowSuggestions(false);
                    }}
                  >
                    <Box
                      className="TOPNAV-search-suggestions-wrapper"
                      sx={{ zIndex: (theme) => theme.zIndex.drawer + 4 }}
                    >
                      <Paper elevation={4}>
                        {suggestions.map((item, index) => {
                          return (
                            <Box
                              key={index}
                              display="flex"
                              p={1}
                              alignItems="center"
                              sx={{
                                cursor: "pointer",
                                "&:hover": {
                                  background: "rgba(0,0,0,0.1)",
                                },
                              }}
                              onClick={() => {
                                setSearchText(item.title.trim());
                                setSearchInput("");
                                setShowSuggestions(false);
                                window.scrollTo({
                                  top: 0,
                                  behavior: "smooth",
                                });
                                navigate("/products");
                              }}

                              // onClick={handleOnSearchButtonClicked}
                            >
                              <img width="50" src={item.img} alt={item.title} />
                              <Typography pl={2}>{item.title}</Typography>
                            </Box>
                          );
                        })}
                      </Paper>
                    </Box>
                  </ClickAwayListener>
                )}
              </Box>
            </form>
          </Grid>

          {/* login | register for larger screen */}
          <Grid
            item
            sm={4}
            container
            justifyContent="flex-end"
            alignItems="center"
            sx={{ display: { xs: "none", md: "flex" } }}
          >
            {isAuth ? (
              <Link
                to="/user"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Box display="flex" alignItems="center">
                  <Avatar src={userData.image} sx={{ width: 24, height: 24 }} />
                  <Typography variant="subtitle1" color="common.white" pl={1}>
                    {userData.firstName === undefined ? (
                      <Skeleton width={100} />
                    ) : (
                      `${userData.firstName} ${userData.lastName}`
                    )}
                  </Typography>
                </Box>
              </Link>
            ) : (
              <Link
                to="/login"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Typography
                  variant="button"
                  color="primary.main"
                  sx={{
                    "&:hover": {
                      color: "common.white",
                    },
                  }}
                >
                  login
                </Typography>
              </Link>
            )}

            <Typography variant="button" color="rgba(255,255,255,0.5)">
              &nbsp;&nbsp; | &nbsp;
            </Typography>

            {isAuth ? (
              <Button
                color="primary"
                sx={{
                  "&:hover": {
                    color: "common.white",
                  },
                }}
                onClick={() => {
                  removeToken();
                }}
              >
                Logout
              </Button>
            ) : (
              <Link
                to="/register"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Typography
                  variant="button"
                  color="primary.main"
                  sx={{
                    "&:hover": {
                      color: "common.white",
                    },
                  }}
                >
                  Register
                </Typography>
              </Link>
            )}
          </Grid>
        </Grid>
      </Box>
    </AppBar>
  );
};

export default TopNav;
