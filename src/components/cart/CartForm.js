import React, { useContext, useState, useEffect } from "react";
import {
  FormControl,
  Paper,
  TextField,
  Typography,
  Button,
  Box,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import cartContext from "../../contexts/cart-context";

const CartForm = ({ id, priceId, image, title, price, setOpenModal }) => {
  const [quantity, setQuantity] = useState(0);
  const [note, setNote] = useState("");

  const [btnLoading, setBtnLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const {
    addToCart,
    add_to_cart_success,
    fetchCart,
    cartItems,
    cartCount,
    cart_message,
  } = useContext(cartContext);

  useEffect(() => {
    if (cartItems !== undefined) {
      let item = cartItems.cartProducts.find((item) => item.product.id == id);

      if (item) {
        setQuantity(item.quantity);
        setNote(item.note);
      }
    }
  }, []);

  useEffect(() => {
    if (add_to_cart_success) {
      console.log("cart form : add to cart success::", add_to_cart_success);
      console.log(cart_message);

      fetchCart();

      setBtnLoading(false);
      setTimeout(() => {
        setOpenModal(false);
      }, 2000);
    }
  }, [add_to_cart_success]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setError(false);
    if (parseInt(quantity) >= 1 && parseInt(quantity) < 100) {
      setBtnLoading(true);
      let body = {
        productId: id,
        priceId: priceId,
        quantity: quantity,
        note: note ? note : "empty",
      };

      addToCart(body);
    } else {
      setError(true);
      setErrorMessage("Quantity must be between 1 to 99");
    }
  };
  return (
    <Paper
      align="center"
      sx={{ padding: 3, width: { xs: 300, md: 400, lg: 500 } }}
    >
      <Box
        border={1}
        borderColor="divider"
        display="flex"
        width="90%"
        alignItems="center"
      >
        <img
          src={image}
          alt={title}
          width={100}
          height={100}
          onError={(e) => {
            e.target.onerror = null;
            e.target.src =
              "https://landerapp.com/blog/wp-content/uploads/2018/05/MAG-FR-Oestreicher-Singer-Product-Recommendation-Viral-Marketing-Social-Media-Network-Ecommerce-1200-1200x627.jpg";
          }}
        />
        <Box
          width="100%"
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <Typography
            variant="h6"
            color="info.main"
            sx={{ fontWeight: "bold" }}
          >
            {title}
          </Typography>
          <Typography variant="overline">
            {Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "NRS",
            }).format(price)}
          </Typography>
        </Box>
      </Box>
      {error && (
        <Typography variant="overline" color="error">
          {errorMessage}
        </Typography>
      )}
      {add_to_cart_success && (
        <Typography variant="overline" color="info.main">
          Added to cart
        </Typography>
      )}
      <form
        noValidate
        autoComplete="off"
        action="submit"
        onSubmit={handleSubmit}
      >
        <FormControl sx={{ width: "50%", marginTop: "50px" }}>
          <TextField
            name="quantity"
            label="Quantity"
            type="number"
            InputProps={{ inputProps: { min: 1, max: 99 } }}
            value={quantity}
            required
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
          />
        </FormControl>

        <FormControl sx={{ width: "50%", margin: "10px" }}>
          <TextField
            name="note"
            label="Note"
            type="text"
            placeholder="Note"
            multiline
            rows={4}
            value={note}
            onChange={(e) => {
              setNote(e.target.value.trim());
            }}
          />
        </FormControl>
        <Box>
          {add_to_cart_success ? (
            <Button disabled>Added to cart</Button>
          ) : (
            <LoadingButton
              loading={btnLoading}
              type="submit"
              variant="contained"
              color="info"
              sx={{ margin: "10px" }}
            >
              Done
            </LoadingButton>
          )}
        </Box>
      </form>
    </Paper>
  );
};

export default CartForm;
