import React, { useContext, useEffect } from "react";
import cartContext from "../../contexts/cart-context";
import {
  Box,
  Divider,
  Grid,
  Typography,
  IconButton,
  Paper,
  Button,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddBoxIcon from "@mui/icons-material/AddBox";
import IndeterminateCheckBoxIcon from "@mui/icons-material/IndeterminateCheckBox";
import "./cart.css";
import { useNavigate } from "react-router-dom";

const Cart = ({ openCart }) => {
  const {
    cartItems,
    cartCount,
    // deleteCart,
    // increaseCartItemCount,
    // decreaseCartItemCount,
  } = useContext(cartContext);

  const navigate = useNavigate();

  useEffect(() => {
    console.log("from cart component: cartItems :: ", cartItems);
  }, [cartItems]);

  return (
    <Paper
      sx={{ borderRadius: "0", minWidth: "450px", maxWidth: "700px" }}
      elevation={24}
    >
      <Paper
        sx={{ backgroundColor: "primary.main", borderRadius: "0" }}
        elevation={4}
      >
        <Typography variant="h6" fontWeight="bold" p={2}>
          Cart ({cartCount})
        </Typography>
      </Paper>
      <Box maxHeight="50vh" sx={{ overflowY: "auto" }}>
        {cartItems.cartProducts.map((item) => {
          return (
            <Box key={item.id} px={2}>
              <Grid
                container
                spacing={2}
                p={1}
                alignItems="center"
                justifyContent="space-between"
              >
                <Grid
                  item
                  container
                  direction="row"
                  alignItems="center"
                  spacing={1}
                  sx={{ width: { xs: "380px", md: "350px" } }}
                >
                  <Grid item>
                    <img
                      className="CARD-image"
                      src={item.product.images[0].imageName}
                      alt={item.product.title}
                    />
                  </Grid>
                  <Grid item width={250}>
                    <Typography variant="subtitle2" fontWeight="bold">
                      {item.product.title}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item container alignItems="center" width={120}>
                  <Grid item>
                    <IconButton
                      color="primary"
                      //disabled={item.itemCount < 2 ? true : false}
                      onClick={() => {
                        // decreaseCartItemCount(item.id);
                      }}
                    >
                      <IndeterminateCheckBoxIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <input
                      disabled
                      value={item.quantity}
                      className="CART-input"
                    />
                  </Grid>
                  <Grid item>
                    <IconButton
                      color="primary"
                      // disabled={item.itemCount > 9 ? true : false}
                      onClick={() => {
                        // increaseCartItemCount(item.id);
                      }}
                    >
                      <AddBoxIcon fontSize="small" />
                    </IconButton>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography color="info.main" fontWeight="bold">
                    {Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "NRS",
                    }).format(item.price)}
                  </Typography>
                </Grid>
                <Grid item>
                  <IconButton
                    aria-label="delete"
                    onClick={() => {
                      // deleteCart(item.id);
                    }}
                  >
                    <DeleteIcon color="error" />
                  </IconButton>
                </Grid>
              </Grid>
              <Divider />
            </Box>
          );
        })}
      </Box>
      <Paper
        sx={{ backgroundColor: "secondary.main", borderRadius: "0" }}
        elevation={4}
      >
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
          p={2}
        >
          <Typography
            align="center"
            color="common.white"
            fontWeight="bold"
            variant="h6"
          >
            Total: &nbsp;
            {Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "NRS",
            }).format(cartItems.subTotal)}
          </Typography>
          {cartCount > 0 && (
            <Button
              variant="contained"
              color="info"
              onClick={() => {
                openCart(false);
                window.scrollTo({
                  top: 0,
                  behavior: "smooth",
                });
                navigate("/checkout");
              }}
            >
              Proceed to checkout
            </Button>
          )}
        </Grid>
      </Paper>
    </Paper>
  );
};

export default Cart;
