import React from "react";
import "./landing.css";

import { Box, Typography } from "@mui/material";

const Landing = (props) => {
  return (
    <Box sx={{ position: "relative" }}>
      <img className="LANDING-image" src={props.item.url} alt="Landing" />
      <Typography variant="h2" className="LANDING-description">
        {props.item.description}
      </Typography>
    </Box>
  );
};

export default Landing;
