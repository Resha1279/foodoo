import React, { useState, useContext } from "react";
import {
  Avatar,
  FormControl,
  Paper,
  TextField,
  Typography,
  Button,
  Divider,
  Box,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { Link, useNavigate } from "react-router-dom";
import authContext from "../../contexts/auth-context";
import { BASE_URL } from "../../constants/constants";
import { useAlert } from "react-alert";

const LoginForm = ({ fromLogin = false, setOpenModal }) => {
  const navigate = useNavigate();
  const alert = useAlert();

  const { setToken } = useContext(authContext);

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const [forgotPassword, sestForgotPassword] = useState(false);
  const [forgotPasswordInput, setForgotPasswordInput] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setError(false);
    let body = {
      client_id: 2,
      client_secret: "ZkPYPKRiUsEzVke7Q5sq21DrVvYmNK5w5bZKGzQo",
      grant_type: "password",
      username: userName,
      password: password,
    };
    let url = `${BASE_URL}/api/v4/auth/login`;

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(body),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("log in Success:", result);
        alert.show("Login Success", { type: "success" });
        console.log("access_token:", result.access_token);
        setToken(result.access_token);

        setIsLoading(false);
        if (fromLogin) {
          window.scrollTo({
            top: 0,
            behavior: "smooth",
          });

          navigate("/");
        } else {
          setOpenModal(false);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        alert.show("Login Failed", { type: "error" });
        setError(true);
        setErrorMessage("Incorrect email or password");

        console.error("Error:", error);
      });
  };

  const handleForgotPasswordSubmit = (e) => {
    e.preventDefault();
    let url = `${BASE_URL}/api/v4/auth/forgot-password`;

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: forgotPasswordInput,
      }),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("forgot password success:", result);
        alert.show("Reset link has been sent to the eamil address", {
          type: "success",
        });
      })
      .catch((error) => {
        alert.show("link failed to send", { type: "error" });

        console.error("Error:", error);
      });
  };

  return (
    <Paper
      align="center"
      sx={{ padding: 3, width: { xs: 300, md: 400, lg: 500 } }}
    >
      <Avatar>
        <LockOutlinedIcon />
      </Avatar>
      <Typography m={2} variant="h5">
        Login
      </Typography>
      {error && (
        <Typography variant="overline" color="error">
          {errorMessage}
        </Typography>
      )}
      <form
        noValidate
        autoComplete="off"
        action="submit"
        onSubmit={handleSubmit}
      >
        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="email"
            label="Email"
            type="email"
            placeholder="Enter Email"
            required
            value={userName}
            onChange={(e) => {
              setUserName(e.target.value.trim());
            }}
          />
        </FormControl>

        <FormControl sx={{ width: "90%", margin: "10px" }}>
          <TextField
            name="password"
            label="Password"
            type="password"
            placeholder="Enter Password"
            required
            value={password}
            onChange={(e) => {
              setPassword(e.target.value.trim());
            }}
          />
        </FormControl>

        <LoadingButton
          loading={isLoading}
          type="submit"
          variant="contained"
          color="info"
          sx={{ margin: "10px" }}
        >
          Login
        </LoadingButton>
      </form>
      <Box>
        <Button
          onClick={() => {
            sestForgotPassword(!forgotPassword);
          }}
          size="small"
          sx={{ marginTop: "10px" }}
        >
          Forgot Password?
        </Button>
        {forgotPassword && (
          <Box sx={{ marginBottom: "10px" }}>
            <form onSubmit={handleForgotPasswordSubmit}>
              <FormControl sx={{ width: "90%", margin: "10px" }}>
                <TextField
                  name="email"
                  label="Email"
                  type="email"
                  placeholder="Enter Email"
                  required
                  helperText=" A link to reset your password will be sent to this email
                address."
                  value={forgotPasswordInput}
                  onChange={(e) => {
                    setForgotPasswordInput(e.target.value.trim());
                  }}
                />
              </FormControl>

              <Button
                size="small"
                color="info"
                variant="contained"
                type="submit"
              >
                OK
              </Button>
            </form>
          </Box>
        )}
      </Box>
      <Divider />
      <Link to="/register">
        <Button size="large" sx={{ marginTop: "20px" }} color="info">
          Create new account
        </Button>
      </Link>
    </Paper>
  );
};

export default LoginForm;
