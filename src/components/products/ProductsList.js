import React, { useContext, useEffect, useState } from "react";
import {
  Grid,
  Box,
  Typography,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";
import ProductCard from "./ProductCard";
import { BASE_URL } from "../../constants/constants";
import productContext from "../../contexts/product-context";
import Title from "../../ui/title/Title";
import Placeholder from "../../ui/placeholder/Placeholder";
import useDocumentTitle from "../../hooks/useDocumentTitle";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";
import "./product-card.css";

const ProductsList = () => {
  const [sortOption, setSortOption] = useState("A");

  const {
    activeCategory,
    fetchProducts,
    products_loading,
    products,
    search,
    search_text,
  } = useContext(productContext);

  const [sortedItems, setSortedItems] = useState([]);

  useEffect(() => {
    console.log("search_text", search_text);
    if (search) {
      fetchProducts(`${BASE_URL}/api/v4/product?query=${search_text}`);
    } else if (activeCategory === "all") {
      fetchProducts(`${BASE_URL}/api/v4/product`);
    } else {
      fetchProducts(
        `${BASE_URL}/api/v4/product?query=&page=1&categoryId=${activeCategory.slug}&allProduct=1`
      );
    }
  }, [activeCategory, search_text]);

  useDocumentTitle(
    `Categories | ${
      search
        ? "Search"
        : activeCategory === "all"
        ? "ALL ITEMS"
        : activeCategory.title
    }`
  );

  useEffect(() => {
    sortItems();
  }, [sortOption, products]);

  const sortItems = () => {
    let filterArray = [...products];
    switch (sortOption) {
      case "A":
        filterArray.sort((a, b) => {
          if (a.title <= b.title) {
            return -1;
          }
          if (a.title > b.title) {
            return 1;
          }
          return 0;
        });
        break;
      case "B":
        filterArray.sort((a, b) => {
          if (a.title <= b.title) {
            return 1;
          }
          if (a.title > b.title) {
            return -1;
          }
          return 0;
        });
        break;
      case "C":
        filterArray.sort(
          (a, b) => a.unitPrice[0].sellingPrice - b.unitPrice[0].sellingPrice
        );
        break;
      case "D":
        filterArray.sort(
          (a, b) => b.unitPrice[0].sellingPrice - a.unitPrice[0].sellingPrice
        );
        break;
      default:
        break;
    }
    setSortedItems(filterArray);
  };

  return (
    <>
      <Box className="PRODUCT-LIST-title-container">
        <Title
          text={
            search
              ? "SEARCH"
              : activeCategory === "all"
              ? "ALL ITEMS"
              : activeCategory.title
          }
          width="40px"
        />
        <Box
          className="container"
          display="flex"
          justifyContent="space-evenly"
          alignItems="center"
        >
          <Box width="100%">
            {search && (
              <Typography color="info.main" variant="subtitle1">
                Showing search results for '{search_text}':{" "}
              </Typography>
            )}
            <Typography color="secondary" variant="overline">
              {products.length} items available
            </Typography>
          </Box>
          <Box
            display="flex"
            justifyContent="end"
            alignItems="center"
            width="100%"
          >
            <Typography color="secondary" pr={1}>
              Sort By:
            </Typography>
            <FormControl sx={{ minWidth: 140 }}>
              <Select
                value={sortOption}
                onChange={(e) => {
                  setSortOption(e.target.value);
                }}
                sx={{ backgroundColor: "white" }}
              >
                <MenuItem value={"A"}>A - Z</MenuItem>
                <MenuItem value={"B"}>Z - A</MenuItem>
                <MenuItem value={"C"}>Price(low to high)</MenuItem>
                <MenuItem value={"D"}>Price(high to low)</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Box>
      </Box>
      <Box className="container">
        {products_loading ? (
          <Placeholder />
        ) : sortedItems.length === 0 || products.length === 0 ? (
          <Box
            height="400px"
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
          >
            <ShoppingBasketIcon
              sx={{ fontSize: "200px", color: "rgba(0,0,0,0.4)" }}
            />
            <Typography align="center" variant="h4" color="rgba(0,0,0,0.4)">
              No products available
            </Typography>
          </Box>
        ) : (
          <Grid container spacing={3} justifyContent="center">
            {sortedItems.map((item) => {
              return (
                <Grid item key={item.id}>
                  <ProductCard {...item} />
                </Grid>
              );
            })}
          </Grid>
        )}
      </Box>
    </>
  );
};

export default ProductsList;
