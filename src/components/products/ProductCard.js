import React, { useContext, useEffect, useState } from "react";
import "./product-card.css";
import { Button, Modal, Typography, Box } from "@mui/material";
import { useNavigate } from "react-router-dom";
import LoginForm from "../loginform/LoginForm";
import authContext from "../../contexts/auth-context";
import CartForm from "../cart/CartForm";

const ProductCard = ({ id, title, unitPrice, images, categoryTitle }) => {
  const { isAuth } = useContext(authContext);

  const [openModal, setOpenModal] = useState(false);
  const navigate = useNavigate();

  const handleOnModalClosed = () => {
    setOpenModal(false);
  };

  return (
    <div className="PRODUCT-CARD-wrapper">
      <Box
        bgcolor="white"
        sx={{ cursor: "pointer" }}
        onClick={() => {
          window.scrollTo({
            top: 0,
            behavior: "smooth",
          });
          navigate(`/products/${id}`);
        }}
      >
        <Box sx={{ overflow: "hidden" }}>
          <img
            src={images[0].imageName}
            alt={title}
            className="PRODUCT-CARD-image"
            onError={(e) => {
              e.target.onerror = null;
              e.target.src =
                "https://landerapp.com/blog/wp-content/uploads/2018/05/MAG-FR-Oestreicher-Singer-Product-Recommendation-Viral-Marketing-Social-Media-Network-Ecommerce-1200-1200x627.jpg";
            }}
          />
        </Box>
        <div className="PRODUCT-CARD-title">
          <Typography
            variant="subtitle1"
            align="center"
            p={2}
            sx={{ fontWeight: "bold", lineHeight: 1.25, fontSize: 14 }}
          >
            {title}
          </Typography>
        </div>
        <Box width="100%" display="flex" justifyContent="center">
          <Typography variant="caption" color="secondary">
            {categoryTitle}
          </Typography>
        </Box>
        <Typography
          variant="h6"
          align="center"
          p={1}
          pt={0}
          color="info.main"
          sx={{ fontWeight: "bold" }}
        >
          {Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "NRS",
          }).format(unitPrice[0].markedPrice)}
        </Typography>
      </Box>
      <Button
        fullWidth
        variant="contained"
        sx={{
          ":hover": {
            bgcolor: "info.main",
            color: "white",
          },
          borderRadius: 0,
        }}
        disableElevation
        onClick={() => {
          setOpenModal(true);
        }}
      >
        Add to cart
      </Button>

      <Modal open={openModal} onClose={handleOnModalClosed}>
        <Box className="MODAL-position">
          {isAuth ? (
            <CartForm
              id={id}
              title={title}
              priceId={unitPrice[0].id}
              image={images[0].imageName}
              price={unitPrice[0].markedPrice}
              setOpenModal={setOpenModal}
            />
          ) : (
            <Box
              p={2}
              display="flex"
              flexDirection="column"
              justifyContent="center"
              bgcolor="info.main"
            >
              <Typography variant="h6" align="center" color="common.white">
                You are not logged in
              </Typography>
              <Typography
                variant="overline"
                color="rgb(255,255,255,0.8)"
                aLign="center"
              >
                Please Login to continue
              </Typography>
              <Box>
                <LoginForm setOpenModal={setOpenModal} />
              </Box>
            </Box>
          )}
        </Box>
      </Modal>
    </div>
  );
};

export default ProductCard;
