import React, { useState } from "react";
import useFetch from "../../hooks/useFetch";
import { BASE_URL, API_KEY } from "../../constants/constants";

const Search = () => {
  const [searchText, setSearchText] = useState("");

  const { data, loading, error } = useFetch(
    `${BASE_URL}/api/v4/suggest?query=${searchText}&page=1`,

    {
      method: "GET",
      headers: {
        "Warehouse-Id": "1",
        "Api-key": API_KEY,
        "Content-Type": "application/json",
      },
      redirect: "follow",
    }
  );

  return <div></div>;
};

export default Search;
