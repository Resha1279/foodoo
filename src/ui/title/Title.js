import React from "react";
import { Typography, Box } from "@mui/material";

const Title = ({ text, width = "200px" }) => {
  return (
    <Box justifyContent="center" my={4}>
      <Typography
        align="center"
        variant="h4"
        color={"text.secondary"}
        sx={{ textTransform: "uppercase", fontWeight: "bold" }}
      >
        {text}
      </Typography>
      <Box
        sx={{
          width: { width },
          height: "2px",
          backgroundColor: "info.main",
          margin: "8px auto",
        }}
      ></Box>
    </Box>
  );
};

export default Title;
