import React from "react";
import { Skeleton, Stack, Box } from "@mui/material";

const PlaceholderCategories = () => {
  return (
    <Box p={4}>
      <Stack spacing={4}>
        <Skeleton variant="text" width={100} />
        <Skeleton variant="text" width={100} />
        <Skeleton variant="text" width={100} />
        <Skeleton variant="text" width={100} />
      </Stack>
    </Box>
  );
};

export default PlaceholderCategories;
