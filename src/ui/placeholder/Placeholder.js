import { Skeleton, Box, Grid, Stack } from "@mui/material";
import React from "react";

const Placeholder = () => {
  return (
    <Grid container spacing={3} justifyContent="center">
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
      <Grid item>
        <Stack spacing={1}>
          <Skeleton variant="rectangular" width={250} height={200} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rectangular" width={250} height={40} />
        </Stack>
      </Grid>
    </Grid>
  );
};

export default Placeholder;
