import React from "react";
import { Skeleton, Stack, Box } from "@mui/material";

const PlaceholderSlider = () => {
  return (
    <Box p={1} display="flex">
      <Skeleton
        variant="image"
        sx={{ marginRight: "8px" }}
        width={250}
        height={400}
      />
    </Box>
  );
};

export default PlaceholderSlider;
