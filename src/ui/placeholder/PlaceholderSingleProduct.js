import React from "react";
import { Skeleton, Stack, Grid, Box } from "@mui/material";

const PlaceholderSingleProduct = () => {
  return (
    <Box className="container" my={4}>
      <Grid container spacing={4}>
        <Grid item xs={12} md={4}>
          <Stack spacing={1}>
            <Skeleton variant="rectangular" height={200} />
          </Stack>
        </Grid>
        <Grid item xs={12} md={8}>
          <Stack spacing={1}>
            <Skeleton variant="text" />
            <Skeleton variant="text" />
            <Skeleton variant="text" />
            <Skeleton variant="text" />
            <Skeleton variant="text" />
            <Skeleton variant="rectangular" width={160} height={40} />
          </Stack>
        </Grid>
      </Grid>
    </Box>
  );
};

export default PlaceholderSingleProduct;
