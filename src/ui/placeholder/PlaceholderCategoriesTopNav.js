import React from "react";
import { Skeleton, Stack, Box } from "@mui/material";

const PlaceholderCategoriesTopNav = () => {
  return (
    <Box p={1} display="flex">
      <Skeleton variant="text" sx={{ marginRight: "8px" }} width={70} />
      <Skeleton variant="text" sx={{ marginRight: "8px" }} width={70} />
      <Skeleton variant="text" sx={{ marginRight: "8px" }} width={70} />
      <Skeleton variant="text" sx={{ marginRight: "8px" }} width={70} />
    </Box>
  );
};

export default PlaceholderCategoriesTopNav;
