import React from "react";
import { Skeleton, Stack, Box } from "@mui/material";
const PlaceholderCategoriesHome = () => {
  return (
    <Box p={1} display="flex">
      <Skeleton
        variant="image"
        sx={{ marginRight: "8px" }}
        width={100}
        height={100}
      />
      <Skeleton
        variant="image"
        sx={{ marginRight: "8px" }}
        width={100}
        height={100}
      />
      <Skeleton
        variant="image"
        sx={{ marginRight: "8px" }}
        width={100}
        height={100}
      />
      <Skeleton
        variant="image"
        sx={{ marginRight: "8px" }}
        width={100}
        height={100}
      />
    </Box>
  );
};

export default PlaceholderCategoriesHome;
