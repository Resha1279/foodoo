import React from "react";
import { Alert, Snackbar } from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
import ErrorIcon from "@mui/icons-material/Error";

const CustomAlert = ({ openAlert, handleClose, message, severity }) => {
  return (
    <Alert
      icon={
        severity === "success" ? (
          <CheckIcon fontSize="inherit" />
        ) : (
          <ErrorIcon fontSize="inherit" />
        )
      }
      onClose={handleClose}
      variant="filled"
      severity={severity}
      open={openAlert}
    >
      {message}
    </Alert>
  );
};

export default CustomAlert;
