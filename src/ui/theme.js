import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    type: "light",
    primary: {
      main: "#fb9735",
    },
    secondary: {
      main: "#333333",
    },
    info: {
      main: "#3399CC",
    },
  },
});
