import React from "react";
import { Box, Typography } from "@mui/material";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";

export const NotFound = () => {
  return (
    <Box
      height="90vh"
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
    >
      <SentimentVeryDissatisfiedIcon
        sx={{ fontSize: "200px", color: "rgba(0,0,0,0.4)" }}
      />
      <Typography align="center" variant="h4" color="rgba(0,0,0,0.4)">
        404 Page Not Found
      </Typography>
    </Box>
  );
};
