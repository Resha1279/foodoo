import React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import "./fallback.css";

export const FallBack = () => {
  return (
    <div className="FALLBACK-container">
      <CircularProgress />
    </div>
  );
};
