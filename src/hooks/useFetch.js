import React, { useState, useEffect } from "react";

const useFetch = (url, header) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    setLoading(true);
    setData([]);
    setError(false);
    fetchData();
  }, [url]);

  const fetchData = () => {
    fetch(url, header)
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        setLoading(false);
        console.log("useFetch Success:", result.data);
        setData(result.data);
      })
      .catch((error) => {
        setLoading(false);
        setError(true);
        console.error("Error:", error);
      });
  };
  return { data, error, loading };
};

export default useFetch;
