import React, { useEffect, useState } from "react";

const useForm = (submitCallBack) => {
  const [state, setState] = useState({
    first_name: "",
    last_name: "",
    mobile_number: "",
    email: "",
    password: "",
  });
  const [valid, setValid] = useState({
    first_name: false,
    last_name: false,
    mobile_number: false,
    email: false,
    password: false,
  });
  const [helperText, setHelperText] = useState({});
  const [initiateValidityCheck, setInitiateValidityCheck] = useState({
    first_name: false,
    last_name: false,
    mobile_number: false,
    email: false,
    password: false,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setInitiateValidityCheck({
      first_name: true,
      last_name: true,
      mobile_number: true,
      email: true,
      password: true,
    });
    checkValidInput(e);
    if (Object.values(valid).every((item) => item === true)) {
      submitCallBack();
    }
  };

  const handleChange = (e) => {
    e.persist();
    setInitiateValidityCheck((state) => ({
      ...state,
      [e.target.name]: true,
    }));
    setState((state) => ({ ...state, [e.target.name]: e.target.value }));
    checkValidInput(e);
  };

  const checkValidInput = (e) => {
    switch (e.target.name) {
      case "email":
        let expression = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (e.target.value.match(expression)) {
          setValid((valid) => ({ ...valid, [e.target.name]: true }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "",
          }));
        } else {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          if (e.target.value.trim() === "") {
            setHelperText((helperText) => ({
              ...helperText,
              [e.target.name]: "This field cannot be empty",
            }));
          } else {
            setHelperText((helperText) => ({
              ...helperText,
              [e.target.name]: `invalid ${e.target.name}`,
            }));
          }
        }

        break;
      case "first_name":
        if (e.target.value.trim() === "") {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "This field cannot be empty",
          }));
        } else {
          setValid((valid) => ({ ...valid, [e.target.name]: true }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "",
          }));
        }

        break;
      case "last_name":
        if (e.target.value.trim() === "") {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "This field cannot be empty",
          }));
        } else {
          setValid((valid) => ({ ...valid, [e.target.name]: true }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "",
          }));
        }

        break;
      case "mobile_number":
        if (e.target.value.trim() === "") {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "This field cannot be empty",
          }));
        } else if (e.target.value.trim().length !== 10) {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "Please enter valid 10 digits mobile number",
          }));
        } else {
          setValid((valid) => ({ ...valid, [e.target.name]: true }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "",
          }));
        }

        break;
      case "password":
        let password_expression =
          /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,30}$/;
        if (e.target.value.match(password_expression)) {
          setValid((valid) => ({ ...valid, [e.target.name]: true }));
          setHelperText((helperText) => ({
            ...helperText,
            [e.target.name]: "",
          }));
        } else {
          setValid((valid) => ({ ...valid, [e.target.name]: false }));
          if (e.target.value.trim() === "") {
            setHelperText((helperText) => ({
              ...helperText,
              [e.target.name]: "This field cannot be empty",
            }));
          } else if (e.target.value.trim().length < 6) {
            setHelperText((helperText) => ({
              ...helperText,
              [e.target.name]: "Weak Password",
            }));
          } else {
            setHelperText((helperText) => ({
              ...helperText,
              [e.target.name]:
                "Password must include at least an uppercase, a lowercase, a number and a special character",
            }));
          }
        }

        break;
      default:
        break;
    }
  };

  return [
    state,
    handleChange,
    handleSubmit,
    initiateValidityCheck,
    valid,
    helperText,
  ];
};

export default useForm;
