import React, { useContext, useReducer } from "react";
import {
  ADD_TO_CART,
  SET_CART,
  INCREASE_CART_ITEM_COUNT,
  DECREASE_CART_ITEM_COUNT,
  DELETE_CART_ITEM,
  DELETE_CART_LOADING,
  CART_LOADING,
  CART_ADD_SUCCESS,
  CART_MESSAGE,
} from "../constants/cart-actions";
import CartContext from "../contexts/cart-context";
import cartReducer from "../reducers/cart-reducer";
import { BASE_URL, API_KEY } from "../constants/constants";
import authContext from "../contexts/auth-context";
import { useAlert } from "react-alert";

const CartProvider = (props) => {
  const defaultState = {
    cartItems: [],
    totalAmount: 0,

    cartCount: 0,
    cart_loading: false,
    add_to_cart_success: undefined,
    cart_message: "",
    delete_cart_loading: false,
  };

  const [state, dispatch] = useReducer(cartReducer, defaultState);

  const { token } = useContext(authContext);

  const alert = useAlert();

  const fetchCart = () => {
    setCartLoading(true);
    const url = `${BASE_URL}/api/v4/cart`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        dispatch({
          type: SET_CART,
          payload: result.data,
        });
        console.log("fetch Cart Success:", result.data);
      })
      .catch((error) => {
        console.error("fetch Cart Error:", error);
      });
  };

  const addToCart = (cartItem) => {
    setCartAddSuccess(false);
    const url = `${BASE_URL}/api/v4/cart-product`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(cartItem),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        setCartAddSuccess(true);
        alert.show("Item added to cart", { type: "success" });
        //  setCartMessage({ status: "success", message: "Item added to cart" });
        setTimeout(() => {
          setCartAddSuccess(undefined);
          //  setCartMessage("");
        }, 3000);
        console.log("update Cart Success:", data);
      })
      .catch((error) => {
        setCartAddSuccess(undefined);
        alert.show("Failed to add item to cart", { type: "error" });
        console.error("update Cart Error:", error);
      });
  };

  const increaseCartItemCount = (id) => {
    dispatch({
      type: INCREASE_CART_ITEM_COUNT,
      payload: id,
    });
  };
  const decreaseCartItemCount = (id) => {
    dispatch({
      type: DECREASE_CART_ITEM_COUNT,
      payload: id,
    });
  };

  const deleteCart = (id) => {
    setdeleteCartLoading(true);
    const url = `${BASE_URL}/api/v4/cart-product/${id}`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      cartProductId: `${id}`,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "DELETE",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.text();
      })
      .then((result) => {
        alert.show("Item Deleted", { type: "success" });
        console.log("delete Cart Success:", result);
        fetchCart();
        setdeleteCartLoading(false);
      })
      .catch((error) => {
        alert.show("Error deleting item", { type: "error" });
        console.error("delete cart Error:", error);
        setdeleteCartLoading(false);
      });
  };

  const setCartLoading = (payload) => {
    dispatch({
      type: CART_LOADING,
      payload,
    });
  };
  const setdeleteCartLoading = (payload) => {
    dispatch({
      type: DELETE_CART_LOADING,
      payload,
    });
  };
  const setCartAddSuccess = (payload) => {
    dispatch({
      type: CART_ADD_SUCCESS,
      payload,
    });
  };
  const setCartMessage = (payload) => {
    dispatch({
      type: CART_MESSAGE,
      payload,
    });
  };

  const checkout = ({ DeliveryId, PaymentMethodId }) => {
    const url = `${BASE_URL}/api/v4/cart/checkout`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
      DeliveryId: DeliveryId,
      PaymentMethodId: PaymentMethodId,
    };
    fetch(url, {
      method: "DELETE",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.text();
      })
      .then((result) => {
        alert.show("Checkout Success", { type: "success" });
        console.log("checkout Success:", result);
        fetchCart();
      })
      .catch((error) => {
        console.error("checkout error:", error);
        alert.show("Checkout Error", { type: "error" });
      });
  };

  return (
    <CartContext.Provider
      value={{
        cartItems: state.cartItems,
        cartCount: state.cartCount,
        totalAmount: state.totalAmount,
        add_to_cart_success: state.add_to_cart_success,
        delete_cart_loading: state.delete_cart_loading,
        cart_message: state.cart_message,
        fetchCart,
        addToCart,
        deleteCart,
        increaseCartItemCount,
        decreaseCartItemCount,
        checkout,
      }}
    >
      {props.children}
    </CartContext.Provider>
  );
};

export default CartProvider;
