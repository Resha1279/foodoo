import React, { useReducer } from "react";
import AuthContext from "../contexts/auth-context";
import { authReducer } from "../reducers/auth-reducer";
import { REMOVE_TOKEN, SET_TOKEN } from "../constants/auth-actions";

const AuthProvider = (props) => {
  const defaultState = {
    isAuth: false,
    token: "",
  };

  const [state, dispatch] = useReducer(authReducer, defaultState);
  const setToken = (payload) => {
    window.localStorage.clear();
    window.localStorage.setItem("token", JSON.stringify(payload));
    dispatch({
      type: SET_TOKEN,
      payload: payload,
    });
  };
  const removeToken = () => {
    window.localStorage.clear();
    dispatch({
      type: REMOVE_TOKEN,
    });
  };
  return (
    <AuthContext.Provider
      value={{
        isAuth: state.isAuth,
        token: state.token,
        setToken,
        removeToken,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};
export default AuthProvider;
