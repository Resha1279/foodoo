import React, { useReducer, useContext } from "react";
import { API_KEY, BASE_URL } from "../constants/constants";
import {
  SET_USER_DATA,
  CHANGE_PASSWORD_SUCCESS,
  SET_DELIVERY_ADDRESSES,
  SET_PAYMENT_METHOD,
} from "../constants/user-actions";
import authContext from "../contexts/auth-context";
import userContext from "../contexts/user-context";
import { useAlert } from "react-alert";

import { userReducer } from "../reducers/user-reducer";

const UserProvider = (props) => {
  const defaultState = {
    userData: [],
    update_password_success: false,
    deliveryAddresses: [],
    paymentMethods: [],
  };

  const [state, dispatch] = useReducer(userReducer, defaultState);

  const { token } = useContext(authContext);
  const alert = useAlert();

  const setUserData = () => {
    const url = `${BASE_URL}/api/v4/profile/show`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        dispatch({
          type: SET_USER_DATA,
          payload: result.data,
        });
        console.log("Get user Success:", result.data);
      })
      .catch((error) => {
        console.error("Get User Error:", error);
      });
  };

  const updateUser = (updatedDetails) => {
    const url = `${BASE_URL}/api/v4/profile`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };

    fetch(url, {
      method: "PATCH",
      headers: header,
      body: JSON.stringify(updatedDetails),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        alert.show("Profile successfully updated", { type: "success" });
        console.log("update user Success:", result.data);
        setUserData();
      })
      .catch((error) => {
        alert.show("Error updating profile", { type: "error" });
        console.error("update User Error:", error);
      });
  };

  const changePassword = (updatedDetails) => {
    const url = `${BASE_URL}/api/v4/profile/change-password`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(updatedDetails),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        alert.show("Password successfully updated", { type: "success" });
        console.log("change password Success:", result);
        setChangePasswordSuccess(true);
      })
      .catch((error) => {
        alert.show("Failed to update password", { type: "error" });
        console.error("update Error:", error);
        setChangePasswordSuccess(false);
      });
  };

  const setChangePasswordSuccess = (payload) => {
    dispatch({
      type: CHANGE_PASSWORD_SUCCESS,
      payload,
    });
  };

  const addDeliveryAddress = (address) => {
    const url = `${BASE_URL}/api/v4/delivery-address`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };

    fetch(url, {
      method: "POST",
      headers: header,
      body: JSON.stringify(address),
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("Add address Success:", result);
        alert.show("Address addded", { type: "success" });
        fetchDeliveryAddresses();
      })
      .catch((error) => {
        alert.show("Failed to add address", { type: "error" });
        console.error("Add addressError:", error);
      });
  };

  const fetchDeliveryAddresses = () => {
    const url = `${BASE_URL}/api/v4/delivery-address`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        dispatch({
          type: SET_DELIVERY_ADDRESSES,
          payload: result.data,
        });
        console.log("fetch delivery address  Success:", result.data);
      })
      .catch((error) => {
        console.error("fetch delivery address error:", error);
      });
  };

  const removeDeliveryAddress = (id) => {
    const url = `${BASE_URL}/api/v4/delivery-address/${id}`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "DELETE",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.text();
      })
      .then((result) => {
        alert.show("Address deleted", { type: "success" });
        console.log("remove delivery address  Success:", result);
        fetchDeliveryAddresses();
      })
      .catch((error) => {
        alert.show("Couldn't delete address", { type: "error" });
        console.error("remove delivery address error:", error);
      });
  };

  const fetchPaymentMethod = () => {
    const url = `${BASE_URL}/api/v4/payment-method`;
    let header = {
      Authorization: `Bearer ${token}`,
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        dispatch({
          type: SET_PAYMENT_METHOD,
          payload: result.data,
        });
        console.log("fetch payment method  Success:", result.data);
      })
      .catch((error) => {
        console.error("fetch payment method error:", error);
      });
  };

  return (
    <userContext.Provider
      value={{
        userData: state.userData,
        update_password_success: state.update_password_success,
        deliveryAddresses: state.deliveryAddresses,
        paymentMethods: state.paymentMethods,
        setChangePasswordSuccess,
        setUserData,
        updateUser,
        changePassword,
        addDeliveryAddress,
        fetchDeliveryAddresses,
        removeDeliveryAddress,
        fetchPaymentMethod,
      }}
    >
      {props.children}
    </userContext.Provider>
  );
};

export default UserProvider;
