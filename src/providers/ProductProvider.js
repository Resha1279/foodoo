import React, { useReducer } from "react";
import { API_KEY, BASE_URL } from "../constants/constants";
import {
  FETCH_CATEGORIES,
  FETCH_PRODUCTS,
  SET_CATEGORY,
  SET_PRODUCTS_LOADING,
  SET_PRODUCT_LOADING,
  SET_CATEGORY_LOADING,
  SET_OFFERS_LOADING,
  FETCH_SINGLE_PRODUCT,
  FETCH_OFFERS,
  SET_SEARCH,
  SET_SEARCH_TEXT,
} from "../constants/products-actions";
import ProductContext from "../contexts/product-context";
import productReducer from "../reducers/product-reducer";

const ProductProvider = (props) => {
  const defaultState = {
    products: [],
    categories: [],
    singleProduct: {},
    offers: [],
    activeCategory: "all",
    products_loading: false,
    product_loading: false,
    categories_loading: false,
    offers_loading: false,
    search: false,
    search_text: "",
  };

  const [state, dispatch] = useReducer(productReducer, defaultState);

  const fetchProducts = (url) => {
    setLoading(SET_PRODUCTS_LOADING, true);
    let header = {
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("getProducts Success:", result.data);
        dispatch({
          type: FETCH_PRODUCTS,
          payload: result.data,
        });
      })
      .catch((error) => {
        setLoading(SET_PRODUCTS_LOADING, false);
        console.error("Error:", error);
      });
  };
  const fetchOffers = (url) => {
    setLoading(SET_OFFERS_LOADING, true);
    let header = {
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("fetchoffers Success:", result.data);
        dispatch({
          type: FETCH_OFFERS,
          payload: result.data,
        });
      })
      .catch((error) => {
        setLoading(SET_OFFERS_LOADING, false);
        console.error("Error:", error);
      });
  };

  const fetchSingleProduct = (id) => {
    const url = `${BASE_URL}/api/v4/product/${id}`;
    setLoading(SET_PRODUCT_LOADING, true);
    let header = {
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("get Single Product Success:", result.data);
        dispatch({
          type: FETCH_SINGLE_PRODUCT,
          payload: result.data,
        });
      })
      .catch((error) => {
        console.error("get Single Product Error:", error);
        setLoading(SET_PRODUCT_LOADING, false);
      });
  };

  const fetchCategories = () => {
    setLoading(SET_CATEGORY_LOADING, true);
    const url = `${BASE_URL}/api/v4/category`;
    let header = {
      "Warehouse-Id": "1",
      "Api-key": API_KEY,
      "Content-Type": "application/json",
    };
    fetch(url, {
      method: "GET",
      headers: header,
      redirect: "follow",
    })
      .then((response) => {
        console.log("response status: ", response.status);

        if (!response.ok) {
          throw new Error("HTTP status " + response.status);
        }
        return response.json();
      })
      .then((result) => {
        console.log("getCategories Success:", result.data);
        dispatch({
          type: FETCH_CATEGORIES,
          payload: result.data,
        });
      })
      .catch((error) => {
        setLoading(SET_CATEGORY_LOADING, false);
        console.error("Category Error:", error);
      });
  };

  const setCategory = (payload) => {
    dispatch({
      type: SET_CATEGORY,
      payload,
    });
  };

  const setLoading = (type, payload) => {
    dispatch({
      type,
      payload,
    });
  };

  const setSearch = (payload) => {
    dispatch({
      type: SET_SEARCH,
      payload,
    });
  };
  const setSearchText = (payload) => {
    dispatch({
      type: SET_SEARCH_TEXT,
      payload,
    });
  };

  return (
    <ProductContext.Provider
      value={{
        products: state.products,
        categories: state.categories,
        offers: state.offers,
        singleProduct: state.singleProduct,
        activeCategory: state.activeCategory,
        product_loading: state.product_loading,
        products_loading: state.products_loading,
        offers_loading: state.offers_loading,
        categories_loading: state.categories_loading,
        search: state.search,
        search_text: state.search_text,
        fetchProducts,
        fetchOffers,
        fetchSingleProduct,
        fetchCategories,
        setCategory,
        setSearch,
        setSearchText,
      }}
    >
      {props.children}
    </ProductContext.Provider>
  );
};

export default ProductProvider;
