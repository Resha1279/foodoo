import { SET_TOKEN, REMOVE_TOKEN } from "../constants/auth-actions";

export const authReducer = (state, action) => {
  switch (action.type) {
    case SET_TOKEN:
      return { ...state, isAuth: true, token: action.payload };

    case REMOVE_TOKEN:
      return { ...state, isAuth: false, token: "" };
    default:
      return state;
  }
};
