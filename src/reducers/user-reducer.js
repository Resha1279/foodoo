import {
  SET_USER_DATA,
  CHANGE_PASSWORD_SUCCESS,
  SET_DELIVERY_ADDRESSES,
  SET_PAYMENT_METHOD,
} from "../constants/user-actions";

export const userReducer = (state, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return { ...state, userData: action.payload };
    case SET_DELIVERY_ADDRESSES:
      return { ...state, deliveryAddresses: action.payload };
    case SET_PAYMENT_METHOD:
      return { ...state, paymentMethods: action.payload };
    case CHANGE_PASSWORD_SUCCESS:
      return { ...state, update_password_success: action.payload };
    default:
      return state;
  }
};
