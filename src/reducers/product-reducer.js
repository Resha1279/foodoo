import {
  FETCH_CATEGORIES,
  FETCH_PRODUCTS,
  FETCH_SINGLE_PRODUCT,
  SET_CATEGORY,
  SET_PRODUCTS_LOADING,
  SET_PRODUCT_LOADING,
  SET_CATEGORY_LOADING,
  SET_OFFERS_LOADING,
  FETCH_OFFERS,
  SET_SEARCH,
  SET_SEARCH_TEXT,
} from "../constants/products-actions";

const productReducer = (state, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS:
      return {
        ...state,
        products: action.payload,
        products_loading: false,
      };

    case FETCH_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
        categories_loading: false,
      };
    case FETCH_SINGLE_PRODUCT:
      return {
        ...state,
        singleProduct: action.payload,
        product_loading: false,
      };
    case FETCH_OFFERS:
      return {
        ...state,
        offers: action.payload,
        offers_loading: false,
      };

    case SET_CATEGORY:
      return {
        ...state,
        search: false,
        search_text: "",
        activeCategory: action.payload,
      };

    case SET_PRODUCTS_LOADING:
      return {
        ...state,
        products_loading: action.payload,
      };
    case SET_PRODUCT_LOADING:
      return {
        ...state,
        product_loading: action.payload,
      };
    case SET_CATEGORY_LOADING:
      return {
        ...state,
        categories_loading: action.payload,
      };
    case SET_OFFERS_LOADING:
      return {
        ...state,
        offers_loading: action.payload,
      };
    case SET_SEARCH:
      return {
        ...state,
        search: action.payload,
      };
    case SET_SEARCH_TEXT:
      return {
        ...state,
        search: true,
        search_text: action.payload,
      };
    default:
      return state;
  }
};

export default productReducer;
