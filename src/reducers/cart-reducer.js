import {
  ADD_TO_CART,
  INCREASE_CART_ITEM_COUNT,
  DECREASE_CART_ITEM_COUNT,
  DELETE_CART_ITEM,
  CART_LOADING,
  SET_CART,
  CART_ADD_SUCCESS,
  DELETE_CART_LOADING,
  CART_MESSAGE,
} from "../constants/cart-actions";

const cartReducer = (state, action) => {
  switch (action.type) {
    case SET_CART:
      return {
        ...state,
        cartItems: action.payload,
        cartCount: action.payload.cartProducts.length,
        cart_loading: false,
      };

    case INCREASE_CART_ITEM_COUNT:
      const increase_index = state.cartItems.findIndex(
        (item) => item.id === action.payload
      );
      let newIncreaseArray = [...state.cartItems];
      let itemCountToIncrease = newIncreaseArray[increase_index].itemCount;
      if (itemCountToIncrease < 10) {
        let newIncreaseAmount =
          parseFloat(state.totalAmount) +
          parseFloat(newIncreaseArray[increase_index].price);
        newIncreaseArray[increase_index].itemCount += 1;
        return {
          ...state,
          cartItems: newIncreaseArray,
          cartCount: state.cartCount + 1,
          totalAmount: newIncreaseAmount.toFixed(2),
        };
      } else {
        return {
          ...state,
        };
      }
    case DECREASE_CART_ITEM_COUNT:
      const decrease_index = state.cartItems.findIndex(
        (item) => item.id === action.payload
      );
      let newDecreaseArray = [...state.cartItems];
      newDecreaseArray[decrease_index].itemCount -= 1;
      let newDecreaseAmount =
        parseFloat(state.totalAmount) -
        parseFloat(newDecreaseArray[decrease_index].price);
      return {
        ...state,
        cartItems: newDecreaseArray,
        cartCount: state.cartCount - 1,
        totalAmount: newDecreaseAmount.toFixed(2),
      };

    case DELETE_CART_ITEM:
      const delete_index = state.cartItems.findIndex(
        (item) => item.id === action.payload
      );
      let delete_itemCount = state.cartItems[delete_index].itemCount;
      let delete_price = state.cartItems[delete_index].price;
      const filteredItems = state.cartItems.filter(
        (item) => item.id !== action.payload
      );
      let totalAmountAfterDelete =
        parseFloat(state.totalAmount) -
        delete_itemCount * parseFloat(delete_price);
      return {
        ...state,
        cartItems: filteredItems,
        cartCount: state.cartCount - delete_itemCount,
        totalAmount: totalAmountAfterDelete.toFixed(2),
      };

    case CART_LOADING:
      return {
        ...state,
        cart_loading: action.payload,
      };
    case CART_ADD_SUCCESS:
      return {
        ...state,
        add_to_cart_success: action.payload,
      };
    case DELETE_CART_LOADING:
      return {
        ...state,
        delete_cart_loading: action.payload,
      };
    case CART_MESSAGE:
      return {
        ...state,
        cart_message: action.payload,
      };

    default:
      return state;
  }
};

export default cartReducer;
